<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\Api\PageController;
use  App\Http\Controllers\Api\UserController;
use  App\Http\Controllers\Api\SitePostController;
use  App\Http\Controllers\Api\CarInformationController;
use  App\Http\Controllers\Api\BookingController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });



// Route::get('/greeting', function () {
//     return 'Hello World';
// });

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('user/register', [UserController::class,'userRegister']);
Route::any('/user/login', [UserController::class,'userLogin']);


Route::middleware(['auth:sanctum','isAPIAdmin'])->group(function () {
    Route::get('/checkingAuthenticated', function(){
        return response()->json(['message' => 'You are in', 'status' => 200],200);
    });
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::any('/user/logout', [UserController::class,'userLogout']);
});

Route::any('user/forget/password', [UserController::class,'userForgetPassword']);
Route::any('change/password/{id?}', [UserController::class,'changePassword']);
Route::any('resent/email/verification/link', [UserController::class,'resentEmailVerificationLink']);
Route::any('match/email/verification', [UserController::class,'matchEmailVerification']);
Route::any('get/profile/{id?}', [UserController::class,'getProfile']);
Route::any('update/profile/{id?}', [UserController::class,'updateProfile']);
Route::any('user/delete/{id?}', [UserController::class,'destroyUser']);
Route::any('update/status/user/{id?}/{status?}', [UserController::class,'updateStatus']);
Route::any('user/list', [UserController::class,'index']);
Route::any('sub/admin/list', [UserController::class,'subAdminList']);

Route::any('/save/document', [UserController::class,'saveDocument']);
Route::any('/document/details/{id?}', [UserController::class,'documentDetail']);

Route::any('/booking', [BookingController::class,'saveBooking']);
Route::any('/get/booking', [BookingController::class,'booking']);
Route::any('/get/user/booking', [BookingController::class,'bookingUser']);

Route::any('/fare', [BookingController::class,'saveFare']);
Route::any('/get/fare', [BookingController::class,'fare']);



Route::any('list/car/info', [CarInformationController::class,'index']);
Route::any('post/car/info', [CarInformationController::class,'create']);
Route::any('car/details/{id?}', [CarInformationController::class,'detail']);
Route::any('update/car/info', [CarInformationController::class,'update']);
Route::any('update/status/car/info/{id?}/{status?}', [CarInformationController::class,'updateStatus']);
Route::any('car/info/delete/{id?}', [CarInformationController::class,'destroyCarInfo']);



Route::post('create/cms/page', [PageController::class,'createPage']);
Route::any('detail/cms/page/{id?}', [PageController::class,'pageDetail']);
Route::any('update/cms/page/{id?}', [PageController::class,'updatePage']);
Route::any('delete/cms/page/{id?}', [PageController::class,'destroyPage']);
Route::any('update/status/cms/page/{id?}/{status?}', [PageController::class,'updateStatus']);
Route::any('page/cms/list', [PageController::class,'listPage']);
Route::any('post/site/delete/{id?}', [SitePostController::class,'destroyPostSite']);
Route::any('update/status/post/site/{id?}/{status?}', [SitePostController::class,'updateStatus']);
Route::any('update/latlong', [UserController::class,'updateLatLong']);
Route::any('update/notification', [UserController::class,'updateNotificationStatus']);
Route::any('create/post/site', [SitePostController::class,'create']);
Route::any('update/post/site/{post_id?}/{user_id?}', [SitePostController::class,'update']);
Route::any('favorite', [SitePostController::class,'favorite']);
Route::any('forget/password', [UserController::class,'forgetPassword']);
Route::any('favorite/list', [SitePostController::class,'favoriteList']);
Route::any('search/item', [SitePostController::class,'searchItem']);
Route::any('list/post/site', [SitePostController::class,'index']);
Route::any('detail/post/site/{post_id?}/{user_id?}', [SitePostController::class,'detail']);
Route::any('details/post/site/{post_id?}/{user_id?}', [SitePostController::class,'detailWeb']);


Route::any('contact', [UserController::class,'contactUs']);
Route::any('contact/list', [UserController::class,'contactList']);
Route::any('contact/detail/{id?}', [UserController::class,'contactDetail']);
Route::any('delete/contact/{id?}', [UserController::class,'deleteContact']);

Route::any('cms/page/{slug?}', [PageController::class,'cmsPage']);


