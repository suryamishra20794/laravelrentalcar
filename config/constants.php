<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 11/4/2016
 * Time: 1:23 PM
 */

return [
    
    'PAGINATION' => 5,
    'APP_NAME' => 'Beebly',
    'JOB' => [
        'IMAGE_FOLDER' => 'job_images',
    ],
    'BLOG' => [
        'IMAGE_FOLDER' => 'blog_images',
    ],
    'ANNOUNCEMENT' => [
        'IMAGE_FOLDER' => 'announcement_images',
    ],
    'USER' => [
        'PROFILE_PIC' => 'profile_pic',
    ],
    'SITE_POST' => [
        'VIDEO' => 'video',
        'DOCUMENT' => 'document',
        'IMAGE' => 'images',
    ],
    'CAR_INFO' => [
        'VIDEO' => 'video',
        'DOCUMENT' => 'document',
        'IMAGE' => 'images',
    ],
    'DOCUMENT' => [
        'DRIVERY_LINCENCE' => 'drivery_lincence',
        'EXPERINCE_LETTER' => 'experince_letter',
        'REGISTRATION_CERTIFICATE' => 'registration_certificate',
    ],
    'LIMIT' => '5',
    'HOME_LIMIT' => '5',
    'COUNTRY_CODE' => '+91',
    'FILE_BASE_URL' => 'http://52.14.125.133/our_doctor/public/',
    'FILE_BASE_LOCAL_URL' => 'http://localhost/our_doctor/public/'

];