<?php

function pr($data, $exit = false) {
    echo "<pre>";
    print_r($data);
    if ($exit) {
        die;
    }
}

function userDefineSort($hospital, $doctor){
    $hospitals = strtotime($hospital['updated_at']);
    $doctors = strtotime($doctor['updated_at']);
    return ($hospitals-$doctors);
}



function getLoginUser(){

    $email = session('hospital_email');
    $hospitalInfo = DB::table('hospitals')->where('email',$email)->first();
    $hospitalId = isset($hospitalInfo) && $hospitalInfo != '' ? $hospitalInfo->id : 0;
    return $hospitalId;
}


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function imageBashUrl($folderName){

    $fileUrl = url($folderName);
    return $fileUrl;
}


function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}



function createThumb($name, $filename, $new_w, $new_h) {

    $found = 0;
    $system = explode('.', $name);

    $echeck = strtolower(end($system));

    if (preg_match('/jpg|jpeg/', $echeck)) {
        $src_img = imagecreatefromjpeg($name);
        $found = 1;
    }

    if (preg_match('/png/', $echeck)) {

        $src_img = imagecreatefrompng($name);

        $found = 1;
    }

    if (preg_match('/gif/', $echeck)) {
        $src_img = imagecreatefromgif($name);
        $found = 1;
    }

    if ($found) {

        $old_x = imagesx($src_img);
        $old_y = imagesy($src_img);

        $ar = $old_x / $old_y;

        if ($old_x > 200) {

            if ($new_w == $new_h) {
                $thumb_w = $new_w;
                $thumb_h = $new_h;
            } else {
                $thumb_w = $new_w;
                $thumb_h = (int)(($old_y / $old_x) * $new_w);
            }
        } else {
            $thumb_w = $old_x;
            $thumb_h = $old_y;
        }

        $dst_img = imagecreatetruecolor($thumb_w, $thumb_h);
        imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y);

        if (preg_match("/png/", $echeck)) {
            imagepng($dst_img, $filename);
        } else if (preg_match('/jpg|jpeg/', $echeck)) {
            imagejpeg($dst_img, $filename, 100);
        } else if (preg_match("/gif/", $echeck)) {
            imagegif($dst_img, $filename);
        }
        imagedestroy($dst_img);
    }

    imagedestroy($src_img);

}

function jsonResponse($status, $statusCode, $message = null, $errors = [], $result = nu) {

    $response = ['success' => $status, 'status_code' => $statusCode];

    if ($message != "") {
        $response['message'] = $message;
    }

    if (count($errors) > 0) {
        $response['errors'] = $errors;
    }

    if (count($result) > 0) {
        $response['result'] = $result;
    }

    //pr($response);
    //die;
    //return \Response::json($response, $statusCode, [], JSON_FORCE_OBJECT);
    return response()->json($response, $statusCode, [], JSON_UNESCAPED_UNICODE);
    //return response()->json($response, $statusCode, [JSON_NUMERIC_CHECK]);
}

function jsonResponseTokenWithData($status, $statusCode, $message = null, $errors = [], $result = null, $userName = '', $userId = '') {

    $response = ['success' => $status, 'status_code' => $statusCode];

    if ($message != "") {
        $response['message'] = $message;
    }

    if (count($errors) > 0) {
        $response['errors'] = $errors;
    }

    if (!empty($result)) {
        $response['result'] = $result;
    }
    if (!empty($userName)) {
        $response['user_name'] = $userName;
    }
    if (!empty($userId)) {
        $response['user_id'] = $userId;
    }
    //pr($response);
    //die;
    //return \Response::json($response, $statusCode, [], JSON_FORCE_OBJECT);
    return response()->json($response, $statusCode, [], JSON_UNESCAPED_UNICODE);
    //return response()->json($response, $statusCode, [JSON_NUMERIC_CHECK]);
}

function jsonResponseToken($status, $statusCode, $message = null, $errors = [], $result = null) {

    $response = ['success' => $status, 'status_code' => $statusCode];

    if ($message != "") {
        $response['message'] = $message;
    }

    if (count($errors) > 0) {
        $response['errors'] = $errors;
    }

    if (!empty($result)) {
        $response['result'] = $result;
    }

    //pr($response);
    //die;
    //return \Response::json($response, $statusCode, [], JSON_FORCE_OBJECT);
    return response()->json($response, $statusCode, [], JSON_UNESCAPED_UNICODE);
    //return response()->json($response, $statusCode, [JSON_NUMERIC_CHECK]);
}

/*function earningJsonResponse($status, $statusCode, $message = null, $errors = [], $result = [], $totalEarning = '', $spendTime = '', $completeTrip = '') {

    $response = ['success' => $status, 'status_code' => $statusCode];

    if ($message != "") {
        $response['message'] = $message;
    }

    if (count($errors) > 0) {
        $response['errors'] = $errors;
    }

    if (count($result) > 0) {
        $response['result'] = $result;
    }
    if($totalEarning != ''){
        $response['total_earning'] = $totalEarning;
    }
    if($spendTime != ''){
        $response['spend_time'] = $spendTime;
    }
    if($completeTrip != ''){
        $response['complete_trip'] = $completeTrip;
    }

    //pr($response);
    //die;
    //return \Response::json($response, $statusCode, [], JSON_FORCE_OBJECT);
    return response()->json($response, $statusCode, [], JSON_UNESCAPED_UNICODE);
    //return response()->json($response, $statusCode, [JSON_NUMERIC_CHECK]);
}*/

function generateOtpToken() {

    $otp = substr(uniqid(rand(), True), 0, 4); //verification token
    return $otp;

}


function sendTextMessage($phone,$message) {
    $phone_no = substr($phone, 1);
    $username = "mlf878";
    $password = "wekendy2016";
    $mobile = $phone_no; // format should be 966xxxxxxxxx please note we only provide GCC countries
    $message = $message;
    $sender = "Solo Taxi";
    $language = "1";
    return true;
    $http = "http://api-server3.com/api/send.aspx?username=".$username."&password=".$password."&mobile=".$mobile."&message=".urlencode($message)."&sender=".$sender."&language=".$language;
    //$strfile = file_get_contents($http);

    return $http;

}


function convertToHoursMins($time) {

    $time = date('H:i:s',strtotime($time, strtotime('midnight'))); // 01:01:01
    return $time;
}


function convertMintsToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}


function sendNotification($id,$deviceToken,$deviceType,$message,$messageType) {
//    pr($id);
//    pr($deviceToken);
//    pr($deviceType);
//    pr($message);
//    pr($messageType);
//    die;
    $ch = curl_init("https://fcm.googleapis.com/fcm/send");

    if($deviceType == 'ios') {
        $data = $id . ',' .$messageType;

        $notification = array(
            'title'     => 'Solo App' ,
            'data'      => $data,
            'text'      => $message,
            'sound'     => 'default',
            'content-available'=> 1
        );
        $arrayToSend = array(
            'to' => $deviceToken,
            'notification' => $notification,
            'sound' => 'default',
            'content-available'=> 1,
            'badge' => 2,
            'priority'=>'high'
        );
    }
    else {
        $data = array (
            'title'        => 'Solo App',
            'id'           => $id,
            'message'      => $message,
            'device_type'  => $deviceType,
            'click_action' => 'OPEN_ACTIVITY_1',
            'type'         => $messageType,
            'smallIcon'    => 'notification_icon',
            'largeIcon'    => 'large_notification_icon',
            'sound'        => 'sound_type_notification',
        );
        $arrayToSend = array(
            'registration_ids' => array($deviceToken),
            'data'=> $data,
        );

    }
    $json = json_encode($arrayToSend);
    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key= AAAAGD-LRAc:APA91bFv_9nPT9O_E9C59K4KXmVmZXyO4jSnmOJCdCI8WzZwyQ0OsR_2nKh0OPu6Kpa1EKkK083kZQqGpNOjk7LyqonfYD5mgUhvsyRrM5HhpdOjdSzQQIkO_sh_YT7tNuRlS3wFZviV'; // key here

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_exec($ch);
    curl_close($ch);

    return true;
}



function haversineGreatCircleDistance($inputs)
{
    // convert from degrees to radians
    $latFrom = $inputs['pick_up_latitude'];
    $lonFrom = $inputs['pick_up_longitude'];
    $latTo   = $inputs['destination_latitude'];
    $lonTo   = $inputs['destination_longitude'];
    $apiKey = \Config::get('constant.SOLO_APP_API_KEY');

    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$latFrom.",".$lonFrom."&destinations=".$latTo.",".$lonTo."&key=".$apiKey."";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
    $dist2 =  ($dist * 1.609344);
    return array('distance' => $dist2, 'time' => $time);

}


function distance($inputs, $unit = 'k') {

    $theta = $inputs['pick_up_longitude'] - $inputs['destination_longitude'];
    $dist = sin(deg2rad($inputs['pick_up_latitude'])) * sin(deg2rad($inputs['destination_latitude'])) +  cos(deg2rad($inputs['pick_up_latitude'])) * cos(deg2rad($inputs['destination_latitude'])) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return ($miles * 1.609344);
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return $miles;
    }
}


function generateCoupon(){

    $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $res = "";
    for ($i = 0; $i < 6; $i++) {
        $res .= $chars[mt_rand(0, strlen($chars)-1)];
    }
    return $res;
}

function commonCouponCal($couponInfo, $fare) {

    if (isset($couponInfo) && $couponInfo->discount_type == 1) {

        $newPrice = $fare - ($fare * ($couponInfo->discount_value) / 100);
        $discountAmount = ($fare * ($couponInfo->discount_value) / 100);

    }
    else if (isset($couponInfo) && $couponInfo->discount_type == 2) {

        $discountAmount = ($couponInfo->discount_value);
        $newPrice = $fare - $couponInfo->discount_value;

    } else {

        $newPrice = $fare;
        $discountAmount = 0;
    }

    $fullFare = $newPrice;

    return array($discountAmount, $fullFare);

}



/*function earningJsonResponse($status, $statusCode, $message = null, $errors = [], $result = [], $totalEarning = '', $spendTime = '', $completeTrip = '') {

    $response = ['success' => $status, 'status_code' => $statusCode];

    if ($message != "") {
        $response['message'] = $message;
    }

    if (count($errors) > 0) {
        $response['errors'] = $errors;
    }

    if (count($result) > 0) {
        $response['result'] = $result;
    }
    if($totalEarning != ''){
        $response['total_earning'] = $totalEarning;
    }
    if($spendTime != ''){
        $response['spend_time'] = $spendTime;
    }
    if($completeTrip != ''){
        $response['complete_trip'] = $completeTrip;
    }

    //pr($response);
    //die;
    //return \Response::json($response, $statusCode, [], JSON_FORCE_OBJECT);
    return response()->json($response, $statusCode, [], JSON_UNESCAPED_UNICODE);
    //return response()->json($response, $statusCode, [JSON_NUMERIC_CHECK]);
}*/









function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
    // Calculate the distance in degrees
    $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));

    // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
    switch($unit) {
        case 'km':
            $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
            break;
        case 'mi':
            $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
            break;
        case 'nmi':
            $distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
    }
    return round($distance, $decimals);
}
