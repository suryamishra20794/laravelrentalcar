<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'favorites';
    protected $fillable = [
        'user_id','site_post_id','status','created_at', 'updated_at'
    ];


    public function SitePost()
    {
        return $this->belongsTo(SitePost::class);
    }

}
