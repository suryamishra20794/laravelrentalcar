<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'blogs';

    protected $fillable = [
        'title','user_id', 'category_id','country_id', 'image','description','status','created_at', 'updated_at'
    ];

    public function blogCategory()
    {
        return $this->belongsTo(Category::class, 'category_id','id');
    }

    public function blogUser()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function blogCountry()
    {
        return $this->belongsTo(Country::class, 'country_id','id');
    }

  
}
