<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SitePost extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'site_posts';

    protected $fillable = [
        'item_type','user_id','project_name','cuyds_of_dirt_min','cuyds_of_dirt_max','address','latitude', 'longitude','description','special_instruction','name','email',
        'phone','created_at', 'updated_at'
    ];


    public function SitePostImage()
    {
        return $this->hasMany(SitePostImage::class);
    }
    
    public function favoritePost()
    {
        return $this->hasMany(Favorite::class);
    }



    public function getPostItemsByLatLong($inputs){
        //dd($inputs);
        //die;

        // WHERE uyds_of_dirt_min <= '. $minDirt .'
        // AND cuyds_of_dirt_max <= '. $maxDirt .'
        // AND WHERE distance <= ' . $distance . '
        // AND status = 1 


        $circle_radius = $inputs['redius']; // 3959 circle in km
        $distance = 50; // 50 km
        $latitude = $inputs['latitude'];
        $longitude = $inputs['longitude'];
        $minDirt = $inputs['cuyds_of_dirt_min'];
        $maxDirt = $inputs['cuyds_of_dirt_max'];
        // return $candidates = \DB::select(
        //     'SELECT * FROM
        //             (SELECT *, (' . $circle_radius . ' * acos(cos(radians(' . $latitude . ')) * cos(radians(latitude)) *
        //             cos(radians(longitude) - radians(' . $longitude . ')) +
        //             sin(radians(' . $latitude . ')) * sin(radians(latitude))))
        //             AS distance
        //             FROM site_posts) AS distances
        //         ORDER BY distance
        //         LIMIT 20;
        //     ');

        $query = SitePost::where('status', 1);
        if (!empty($minDirt)) {
            $query->where('cuyds_of_dirt_min', '>=',  $minDirt);
        }
        if (!empty($maxDirt)) {
            $query->where('cuyds_of_dirt_max', '<=',  $maxDirt);
        }
        if (!empty($inputs['type'])) {
            $query->where('item_type', $inputs['type']);
        }
        $result= $query->get();

        return $result;
    }
   
}
