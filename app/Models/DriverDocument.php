<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverDocument extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'driver_documents';

    protected $fillable = [
        'user_id', 'drivery_lincence','experince_letter', 'registration_certificate','additional_infomation','created_at', 'updated_at'
    ];

    use HasFactory;
}
