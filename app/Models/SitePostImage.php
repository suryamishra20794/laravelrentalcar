<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SitePost;

class SitePostImage extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'site_post_images';

    protected $fillable = [
        'site_post_id','image','created_at', 'updated_at'
    ];

    public function SitePost()
    {
        return $this->belongsTo(SitePost::class);
    }
}
