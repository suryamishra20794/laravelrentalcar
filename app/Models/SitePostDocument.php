<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitePostDocument extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'site_post_documents';

    protected $fillable = [
        'site_post_id','document','created_at', 'updated_at'
    ];
}