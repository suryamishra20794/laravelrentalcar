<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fares extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'fares';
    protected $fillable = [
        'base_price','rate_per_km','created_at', 'updated_at'
    ];
  
    use HasFactory;
}
