<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function businesses() {
        return $this->belongsToMany(Business::class);
    }



    public function clientMeets()
    {
        return $this->belongsTo(ClientMeet::class);
    }
}
