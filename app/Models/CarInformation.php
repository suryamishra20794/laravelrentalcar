<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarInformation extends Model
{
    use HasFactory;


        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','car_name','car_number', 'car_image','car_model_number', 'seats','fuel_type','satellite_navigation','air_conditioning','automatic_transmission','how_many_year_old','additional_information', 'mileage', 'location', 'longitude', 'latitude','status'
    ];




    
}
