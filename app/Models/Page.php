<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pages';

    protected $fillable = [
        'title','slug', 'url','description','status','created_at', 'updated_at'
    ];

}


