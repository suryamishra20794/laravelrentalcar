<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'jobs';

    protected $fillable = [
        'title','user_id', 'category_id','country_id', 'job_type', 'image','location','city','zipcode', 'address', 'latitude', 'latitude', 'description','status','created_at', 'updated_at'
    ];

    public function jobCategory()
    {
        return $this->belongsTo(Category::class, 'category_id','id');
    }

    public function jobUser()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function jobCountry()
    {
        return $this->belongsTo(Country::class, 'country_id','id');
    }

}
