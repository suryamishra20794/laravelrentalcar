<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;


class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','dob', 'phone_number','phone_code', 'email','country','state','city','street_address','street_address_2','image','zipcode', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getExistUserEmail($inputs){

        return $this->select('phone_number','email','id','first_name','last_name')->where('email', '=', $inputs['email'])->first();
    }

    public function getExistUserMobileNumber($inputs){

        return $this->select('phone_number','email')->where('phone_number', '=', $inputs['phone_number'])->first();
    }

    public function getLogin($inputs){

        return
            $this
                ->select('id','first_name','user_role','last_name','status','email','password','phone_number','phone_code','state','phone_number','image','dob','latitude','longitude','country','city','zipcode','street_address','street_address_2','alert_message_type','email_verified','notification_status','phone_verified','otp','device_type','device_token','created_at','updated_at')
                ->where([['email', '=', $inputs['email']]])
                ->first();

    }
    public function getLoginUser($inputs){

        return  $this
        ->select('id','first_name','user_role','last_name','email','user_type','device_type','password','phone_number','phone_code','phone_number','image','state','dob','latitude','longitude','country','city','zipcode','street_address','street_address_2','alert_message_type','email_verified','notification_status','phone_verified','otp','status','device_type','device_token','created_at','updated_at')
        ->where('id', '=', $inputs['user_id'])->first();

    }

    public function getVerificationUser($inputs){

        return  $this
        ->select('id','first_name','user_role','last_name','email','password','phone_number','phone_code','phone_number','image','state','dob','latitude','longitude','country','city','zipcode','street_address','street_address_2','alert_message_type','email_verified','notification_status','phone_verified','otp','status','device_type','device_token','created_at','updated_at')
            ->where([['id', '=', $inputs['user_id']], ['otp', '=', trim($inputs['otp'])]])->first();

    }

    public function getRegisterUserInfo($userId){

        return  $this
        ->select('id','first_name','user_role','device_type','user_type','last_name','email','password','phone_number','phone_code','phone_number','image','state','dob','latitude','longitude','country','city','zipcode','street_address','street_address_2','alert_message_type','email_verified','notification_status','phone_verified','otp','status','device_type','device_token','created_at','updated_at')
        ->where('id', '=', $userId)->first();

    }

    public function matchDiviceToken($inputs){

        return  $this
            ->select('status')
            ->where('id', '=', $inputs['user_id'])
            ->where('device_token', '=', $inputs['device_token'])
            ->first();
    }

    public function getLoginDriverIdLatLong($inputs){

        return  $this
            ->select('id','first_name','last_name','email','phone_number','phone_code','phone_number','image','latitude','longitude','country','city','notification_status','phone_verified','otp','device_type','device_token','status','created_at','updated_at')
            ->where('id', '=', $inputs['user_id'])->first();
    }

}
