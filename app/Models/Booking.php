<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'bookings';

    protected $fillable = [ 
        'booking_id','user_id','pick_up_location','drop_off_location','pick_up_date','drop_off_date','pick_up_time','drop_off_time','adults','kids','lat','long','distance','car_info_id','booking_days', 'total_amount','token_amount','pending_balance','payment_type','net_payable_amount','taxes_and_fees','status','created_at', 'updated_at'
    ];


    use HasFactory;
}
