<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SitePostVideo extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'site_post_videos';

    protected $fillable = [
        'site_post_id','video','created_at', 'updated_at'
    ];
}
