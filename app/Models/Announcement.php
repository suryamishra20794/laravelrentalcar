<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'announcements';

    protected $fillable = [
        'title','image','description','status','created_at', 'updated_at'
    ];


  
}
