<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SitePost;
use App\Models\Favorite;
use App\Models\SitePostImage;
use App\Models\SitePostVideo;
use App\Models\SitePostDocument;
use Hash;
use DB;
use League\Flysystem\Exception;
use Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth; 

class SitePostController extends Controller
{
     
    /**
     * @index Site post method
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        try {
            $siteData = SitePost::orderBy('id', 'DESC')->get();
            $siteList = [];
            $images = [];
            if(!empty($siteData)) {
                foreach($siteData as $key  => $site) {
                    $imageData = $site->SitePostImage->whereNotNull('image')->toArray();
                    $imgArr = array_column($imageData, 'image');
                        $siteList[$key] = [
                            'id' => $site->id,
                            'item_type' => $site->item_type,
                            'project_name' => $site->project_name,
                            'cuyds_of_dirt_min' => $site->cuyds_of_dirt_min,
                            'cuyds_of_dirt_max' => $site->cuyds_of_dirt_max,
                            'address' => $site->address,
                            'created_at' => $site->created_at,
                            'images' => $imgArr, // or by using in side another for eachloop  $images,
                            'latitude' => $site->latitude,
                            'longitude' => $site->longitude,
                            'description' => $site->description,
                            'video' => $site->video,
                            'document' => $site->document,
                            'status' => $site->status,
                            'special_instruction' => $site->special_instruction,
                        ];
                } 
                if (!empty($siteList)) {
                    return jsonResponse(true, 200, "List of site post", [], $siteList);
                } else {
                    return jsonResponse(false, 200, "No site data found",[],[]);
                }
            } else {
                return jsonResponse(false, 200, "No site data found",[],[]);
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            //something went wrong
        }
    }


    /**
     * @Detail Site post method
     * @param Request $request
     * @return mixed
     */

    public function detail(Request $request, $post_id = null, $user_id = null){
        try {
            $inputs = $request->all();
            if(!empty($inputs['site_post_id']) && $inputs['site_post_id'] > 0){
                $inputs['site_post_id'];
            } else {
                $inputs['site_post_id'] = $post_id;
            }
            if(!empty($inputs['user_id']) && $inputs['user_id'] > 0){
                $inputs['user_id'];
            } else {
                $inputs['user_id'] = $user_id;
            }
            $validator = Validator::make($inputs, [
                'site_post_id' => 'required',
            ]);
            //dd($inputs);
            if($validator->fails()){
                return jsonResponse(false, 200, "",$validator->errors(),[]);
            }
            $siteData = SitePost::where('id',$inputs['site_post_id'])->first();
            if($siteData){
                $status = '0';
                if($inputs['user_id'] && $inputs['user_id'] > 0) {
                    $favoritePosts = Favorite::where('user_id',$inputs['user_id'])->where('site_post_id',$siteData->id)->first();
                    $status = !empty($favoritePosts) ? $favoritePosts->status  : '0';
                }
                $imgArr = SitePostImage::where('site_post_id', $siteData->id)->pluck('image');
                $siteList = [
                    'id' => $siteData->id,
                    'item_type' => $siteData->item_type,
                    'project_name' => $siteData->project_name,
                    'cuyds_of_dirt_min' => $siteData->cuyds_of_dirt_min,
                    'cuyds_of_dirt_max' => $siteData->cuyds_of_dirt_max,
                    'address' => $siteData->address,
                    'latitude' => $siteData->latitude,
                    'longitude' => $siteData->longitude,
                    'description' => $siteData->description,
                    'video' => $siteData->video,
                    'document' => $siteData->document,
                    'special_instruction' => $siteData->special_instruction,
                    'name' => $siteData->name,
                    'email' => $siteData->email,
                    'phone' => $siteData->phone,
                    'is_fav' => $status,
                    'status' => $siteData->status,
                    'images' => $imgArr
                ];
                if (!empty($siteList)) {
                    return jsonResponse(true, 200, "Site post details", [], $siteList);
                } else {
                    return jsonResponse(false, 200, "No Site Post Id Found",[],[]);
                }
            }
            else {
                return jsonResponse(false, 200, "No Site Post Id Found",[],[]);
            }
  
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }



     /**
     * @Detail Site post method
     * @param Request $request
     * @return mixed
     */
    public function detailWeb(Request $request, $post_id = null, $user_id = null){
        try {
            $inputs = $request->all();
            if(!empty($inputs['site_post_id']) && $inputs['site_post_id'] > 0){
                $inputs['site_post_id'];
            } else {
                $inputs['site_post_id'] = $post_id;
            }
            if(!empty($inputs['user_id']) && $inputs['user_id'] > 0){
                $inputs['user_id'];
            } else {
                $inputs['user_id'] = $user_id;
            }
            $validator = Validator::make($inputs, [
                'site_post_id' => 'required',
            ]);
            //dd($inputs);
            if($validator->fails()){
                return jsonResponse(false, 200, "",$validator->errors(),[]);
            }

            $siteData = SitePost::where('id',$inputs['site_post_id'])->first();
            if($siteData){
                $status = '0';
                if($inputs['user_id'] && $inputs['user_id'] > 0) {
                    $favoritePosts = Favorite::where('user_id',$inputs['user_id'])->where('site_post_id',$siteData->id)->first();
                    $status = !empty($favoritePosts) ? $favoritePosts->status  : '0';
                }

                $imgArr = SitePostImage::where('site_post_id', $siteData->id)->pluck('image');
                $imgArr[] = $siteData->video;
                $siteList = [
                    'id' => $siteData->id,
                    'item_type' => $siteData->item_type,
                    'project_name' => $siteData->project_name,
                    'cuyds_of_dirt_min' => $siteData->cuyds_of_dirt_min,
                    'cuyds_of_dirt_max' => $siteData->cuyds_of_dirt_max,
                    'address' => $siteData->address,
                    'latitude' => $siteData->latitude,
                    'longitude' => $siteData->longitude,
                    'description' => $siteData->description,
                    'video' => $siteData->video,
                    'document' => $siteData->document,
                    'special_instruction' => $siteData->special_instruction,
                    'name' => $siteData->name,
                    'email' => $siteData->email,
                    'phone' => $siteData->phone,
                    'is_fav' => $status,
                    'status' => $siteData->status,
                    'images' => $imgArr
                ];

                if (!empty($siteList)) {
                    return jsonResponse(true, 200, "Site post details", [], $siteList);
                } else {
                    return jsonResponse(false, 200, "No Site Post Id Found",[],[]);
                }
    
            }
            else {
                return jsonResponse(false, 200, "No Site Post Id Found",[],[]);
            }
  
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }


    /**
     * @Create Site post method
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'user_id' => 'required',
                    'item_type' => 'required',
                    'project_name' => 'required',
                    'cuyds_of_dirt_min' => 'required|string',
                    'cuyds_of_dirt_max' => 'required|string',
                    'address' => 'required|string',
                    'latitude' => 'required|string',
                    'longitude' => 'required|string',
                    'description' => 'required|string',
                    'image' => 'required',
                    'image.*' => 'mimes:jpg,jpeg,png,bmp|max:20000',
                    //'video' =>  'mimes:mp4,mov,ogg,qt |required| max:20000',
                    'document' => 'mimes:pdf,doc,ppt,xls,docx,pptx,xlsx,rar,zip|required|max:20000',
                    'special_instruction' => 'required',
                    'name' => 'required',
                    'email' => 'required',
                    'phone' => 'required'
                ],[
                    'image.*.mimes' => 'Only jpeg,png and bmp images are allowed',
                    'image.*.max' => 'Sorry! Maximum allowed size for an image is 20MB',
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $video = $request->file('video');
                $document = $request->file('document');
                $image = $request->file('image');

              


                if (!empty($video)) {
                    $input['video_name'] = time() . '.' . $video->getClientOriginalExtension();
                    $videoPath = public_path(\Config::get('constants.SITE_POST.VIDEO'));
                    $video->move($videoPath, $input['video_name']);
                }
                $videoPath = \Config::get('constants.SITE_POST.VIDEO');
                $videoUrl = imageBashUrl($videoPath);
                if (!empty($document)) {
                    $input['document_name'] = time() . '.' . $document->getClientOriginalExtension();
                    $destinationPath = public_path(\Config::get('constants.SITE_POST.DOCUMENT'));
                    $document->move($destinationPath, $input['document_name']);
                }
                $documentPath = \Config::get('constants.SITE_POST.DOCUMENT');
                $documentUrl = imageBashUrl($documentPath);

            
                $arrSitePost = array(
                    'user_id' => isset($inputs['user_id']) && $inputs['user_id'] != '' ? $inputs['user_id'] : '',
                    'item_type' => isset($inputs['item_type']) && $inputs['item_type'] != '' ? $inputs['item_type'] : '',
                    'project_name' => isset($inputs['project_name']) && $inputs['project_name'] != '' ? $inputs['project_name'] : '',
                    'cuyds_of_dirt_min' => isset($inputs['cuyds_of_dirt_min']) && $inputs['cuyds_of_dirt_min'] != '' ? $inputs['cuyds_of_dirt_min'] : '',
                    'cuyds_of_dirt_max' => isset($inputs['cuyds_of_dirt_max']) && $inputs['cuyds_of_dirt_max'] != '' ? $inputs['cuyds_of_dirt_max'] : '',
                    'address' => isset($inputs['address']) && $inputs['address'] != '' ? $inputs['address'] : '',
                    'description' => isset($inputs['description']) && $inputs['description'] != '' ? $inputs['description'] : '',
                    'special_instruction' => isset($inputs['special_instruction']) && $inputs['special_instruction'] != '' ? $inputs['special_instruction'] : '',
                    'name' => isset($inputs['name']) && $inputs['name'] != '' ? $inputs['name'] : '',
                    'email' => isset($inputs['email']) && $inputs['email'] != '' ? $inputs['email'] : '',
                    'phone' => isset($inputs['phone']) && $inputs['phone'] != '' ? $inputs['phone'] : '',
                    'latitude' => isset($inputs['latitude']) && $inputs['latitude'] != '' ? $inputs['latitude'] : '',
                    'longitude' => isset($inputs['longitude']) && $inputs['longitude'] != '' ? $inputs['longitude'] : '',
                    'video' =>  isset($input['video_name']) && $input['video_name'] != '' ? $videoUrl . '/' . $input['video_name'] : '',
                    'document' => isset($input['document_name']) && $input['document_name'] != '' ? $documentUrl . '/' . $input['document_name'] : '',
                    'created_at' => date('y-m-d h:i:s'),
                    'updated_at' => date('y-m-d h:i:s')
                );

                $lastInsertId = (new SitePost())->insertGetId($arrSitePost);
               
                if (!empty($image)) {
                    $imagePath = \Config::get('constants.SITE_POST.IMAGE');
                    $imageUrl = imageBashUrl($imagePath);
                    
                     
                     
                    //foreach ($image as $imageVal) {
                    	
                        $input['image_name'] = time(). '.' . $image->getClientOriginalExtension();
                        $imagePath = public_path(\Config::get('constants.SITE_POST.IMAGE'));
                        $image->move($imagePath, $input['image_name']);
                        $postImage = !empty($input['image_name']) ? $imageUrl . '/' . $input['image_name'] : '';
                        $createdImages = (new SitePostImage())->create(['site_post_id' => $lastInsertId, 'image' => $postImage]);
                    //}
                }
                DB::commit();
                if (!empty($lastInsertId)) {
                    return jsonResponse(true, 200, "Your site post created successfully", [], []);
                } else {
                    return jsonResponse(false, 200, "Your site post could not be created successfully?",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }


    /**
     * @Update Site post method
     * @param Request $request
     * @return mixed
     */

    public function update(Request $request, $post_id= null, $user_id = null)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();

                //dd($inputs);
                
                if(!empty($inputs['site_post_id']) && $inputs['site_post_id'] > 0){
                    $inputs['site_post_id'];
                } else {
                    $inputs['site_post_id'] = $post_id;
                }
                if(!empty($inputs['user_id']) && $inputs['user_id'] > 0){
                    $inputs['user_id'];
                } else {
                    $inputs['user_id'] = $user_id;
                }

                if(empty($inputs['image'])){
                    unset($inputs['image']);
                }
                if(empty($inputs['video'])){
                    unset($inputs['video']);
                }
                if(empty($inputs['document'])){
                    unset($inputs['document']);
                }

                $validator = Validator::make($inputs, [
                    'site_post_id' => 'required',
                    'user_id' => 'required',
                    'item_type' => 'required',
                    'project_name' => 'required',
                    'cuyds_of_dirt_min' => 'required|string',
                    //'cuyds_of_dirt_max' => 'required|string',
                    'address' => 'required|string',
                    'latitude' => 'required|string',
                    'longitude' => 'required|string',
                    'description' => 'required|string',
                    'image.*' =>    'mimes:jpeg,jpg,png,gif|max:20000',
                    'video' =>    'mimes:mp4,mov,ogg,qt| max:20000',
                    'document' => 'mimes:pdf,doc,ppt,xls,docx,pptx,xlsx,rar,zip|max:20000',
                    'special_instruction' => 'required',
                    'name' => 'required|string',
                    'email' => 'required|email',
                    'phone' => 'required'
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $getSitePostInfo = (new SitePost())->where('id', $inputs['site_post_id'])->first()->toArray();
                if(!empty($getSitePostInfo)){
                    $video = $request->file('video');
                    $document = $request->file('document');
                    $image = $request->file('image');
                    
                    if (!empty($video)) {
                        $input['video_name'] = time() . '.' . $video->getClientOriginalExtension();
                        $videoPath = public_path(\Config::get('constants.SITE_POST.VIDEO'));
                        $video->move($videoPath, $input['video_name']);
                    }
                    $videoPath = \Config::get('constants.SITE_POST.VIDEO');
                    $videoUrl = imageBashUrl($videoPath);
                    if (!empty($document)) {
                        $input['document_name'] = time() . '.' . $document->getClientOriginalExtension();
                        $destinationPath = public_path(\Config::get('constants.SITE_POST.DOCUMENT'));
                        $document->move($destinationPath, $input['document_name']);
                    }
                    $documentPath = \Config::get('constants.SITE_POST.DOCUMENT');
                    $documentUrl = imageBashUrl($documentPath);


                    if (!empty($image)) {
                        $imagePath = \Config::get('constants.SITE_POST.IMAGE');
                        $imageUrl = imageBashUrl($imagePath);
                        $deletedPost = (new SitePostImage())->where('site_post_id', $inputs['site_post_id'])->delete();
                        foreach ($image as $imageVal) {
                            $input['image_name'] = time(). uniqid() . '.' . $imageVal->getClientOriginalExtension();
                            $imagePath = public_path(\Config::get('constants.SITE_POST.IMAGE'));
                            $imageVal->move($imagePath, $input['image_name']);
                            $postImage = !empty($input['image_name']) ? $imageUrl . '/' . $input['image_name'] : $getSitePostInfo['image'];
                            $createdImages = (new SitePostImage())->create(['site_post_id' => $inputs['site_post_id'], 'image' => $postImage]);
                        }
                    }

                
                    $arrSitePost = array(
                        'user_id' => isset($inputs['user_id']) && $inputs['user_id'] != '' ? $inputs['user_id'] : $getSitePostInfo['user_id'],
                        'item_type' => isset($inputs['item_type']) && $inputs['item_type'] != '' ? $inputs['item_type'] : $getSitePostInfo['item_type'],
                        'project_name' => isset($inputs['project_name']) && $inputs['project_name'] != '' ? $inputs['project_name'] : $getSitePostInfo['project_name'],
                        'cuyds_of_dirt_min' => isset($inputs['cuyds_of_dirt_min']) && $inputs['cuyds_of_dirt_min'] != '' ? $inputs['cuyds_of_dirt_min'] : $getSitePostInfo['cuyds_of_dirt_min'],
                        'cuyds_of_dirt_max' => isset($inputs['cuyds_of_dirt_max']) && $inputs['cuyds_of_dirt_max'] != '' ? $inputs['cuyds_of_dirt_max'] : $getSitePostInfo['cuyds_of_dirt_max'],
                        'address' => isset($inputs['address']) && $inputs['address'] != '' ? $inputs['address'] : $getSitePostInfo['address'],
                        'description' => isset($inputs['description']) && $inputs['description'] != '' ? $inputs['description'] : $getSitePostInfo['description'],
                        'special_instruction' => isset($inputs['special_instruction']) && $inputs['special_instruction'] != '' ? $inputs['special_instruction'] : $getSitePostInfo['special_instruction'],
                        'name' => isset($inputs['name']) && $inputs['name'] != '' ? $inputs['name'] : $getSitePostInfo['name'],
                        'email' => isset($inputs['email']) && $inputs['email'] != '' ? $inputs['email'] : $getSitePostInfo['email'],
                        'phone' => isset($inputs['phone']) && $inputs['phone'] != '' ? $inputs['phone'] : $getSitePostInfo['phone'],
                        'latitude' => isset($inputs['latitude']) && $inputs['latitude'] != '' ? $inputs['latitude'] : $getSitePostInfo['latitude'],
                        'longitude' => isset($inputs['longitude']) && $inputs['longitude'] != '' ? $inputs['longitude'] : $getSitePostInfo['longitude'],
                        'video' =>  isset($input['video_name']) && $input['video_name'] != '' ? $videoUrl . '/' . $input['video_name'] : $getSitePostInfo['video'],
                        'document' => isset($input['document_name']) && $input['document_name'] != '' ? $documentUrl . '/' . $input['document_name'] : $getSitePostInfo['document'],
                        'created_at' => date('y-m-d h:i:s'),
                        'updated_at' => date('y-m-d h:i:s')
                    );
                    
                    $updatedPost = (new SitePost())->where('id', $inputs['site_post_id'])->update($arrSitePost);
                    DB::commit();
                    if (!empty($updatedPost) && !empty($updatedPost)) {
                        return jsonResponse(true, 200, "Your site post updated successfully", [], []);
                    } else {
                        return jsonResponse(false, 200, "Your site post could not be updated successfully?",[],[]);
                    }
                } else {
                return jsonResponse(false, 200, "No Site Post Id Found");
            }
        }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            echo $e->getLine();
            die;
            // something went wrong
        }
    }


    public function favorite(Request $request) {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'user_id' => 'required',
                    'site_post_id' => 'required',
                    'status' => 'required',
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }

                $favorite = Favorite::where('user_id',$inputs['user_id'])
                ->where('site_post_id',$inputs['site_post_id'])->first();

                $userInfo = (new User())->find($inputs['user_id']);
                $getSitePostInfo = (new SitePost())->where('id', $inputs['site_post_id'])->first();

                if(!empty($getSitePostInfo) && !empty($userInfo)){

                    if(!empty($favorite)) {
                        $status = !empty($favorite) && $favorite->status == '1' ? '0' : '1';
                        $inputs['status'] = $status;
                        $updateFavorite = (new Favorite())->where('id',$favorite->id)->update($inputs);   
                    } else {
                        $insertedFavorite = (new Favorite())->create($inputs);
                    }
                    if ($inputs['status'] == '1'){
                        if (!empty($insertedFavorite) || !empty($updateFavorite)) {
                            DB::commit();
                                return jsonResponse(true, 200, "Item favorite has been successfully",[],[]);
                        } else {
                            return jsonResponse(false, 200, "Item could not favorite successfully?",[],[]);
                        }
                    } else {
                        if (!empty($insertedFavorite) || !empty($updateFavorite)) {
                            DB::commit();
                                return jsonResponse(true, 200, "Item unfavorite has been successfully",[],[]);
                        } else {
                            return jsonResponse(false, 200, "Item could not unfavorite successfully?",[],[]);
                        }
                    }
              
                } else {
                    return jsonResponse(false, 200, "No Site Post/User Id Found",[],[]);
                }
                
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }



    /**
     * @index Site post method
     * @param Request $request
     * @return mixed
     */
    public function favoriteList(Request $request){

        try {
            $inputs = $request->all();
            $validator = Validator::make($inputs, [
                'user_id' => 'required',
            ]);
            if($validator->fails()){
                return jsonResponse(false, 200, "",$validator->errors(),[]);
            }
            $siteData = Favorite::select('site_posts.*','favorites.status as is_fav')
            ->join('site_posts', 'site_posts.id', '=', 'favorites.site_post_id')
            ->where('favorites.user_id', $inputs['user_id'])
            ->where('favorites.status', '1')
            ->get();
            $siteList = [];
            $images = [];
            if(!empty($siteData)) {
                foreach($siteData as $key => $site) {
                    $imgArr = SitePostImage::where('site_post_id', $site->id)->pluck('image');
                    $siteList[$key] = [
                        'id' => $site->id,
                        'item_type' => $site->item_type,
                        'project_name' => $site->project_name,
                        'cuyds_of_dirt_min' => $site->cuyds_of_dirt_min,
                        'cuyds_of_dirt_max' => $site->cuyds_of_dirt_max,
                        'address' => $site->address,
                        'description' => $site->description,
                        'is_fav' => $site->is_fav,
                        'latitude' => $site->latitude,
                        'longitude' => $site->longitude,
                        'images' => $imgArr // or by using in side another for eachloop  $images,
                    ];
                    
                } 
                if (!empty($siteList)) {
                    return jsonResponse(true, 200, "List of your favorite site post", [], $siteList);
                } else {
                    return jsonResponse(false, 200, "No Site/User Id Found",[],[]);
                }
            }
            else {
                return jsonResponse(false, 200, "No Site/User Id Found",[],[]);
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }




    /**
     * @Seatch item post method
     * @param Request $request
     * @return mixed
     */
    public function searchItem(Request $request){
        try {
            $inputs = $request->all();
            // $validator = Validator::make($inputs, [
            //     'location' => 'required',
            //     'redius' => 'required|integer',
            //     'type' => 'required',
            //     'cuyds_of_dirt_min' => 'required|integer',
            //     'cuyds_of_dirt_max' => 'required|integer',
            // ]);
            // if($validator->fails()){
            //     return jsonResponse(false, 200, "",$validator->errors(),[]);
            // }
            $siteData = (new SitePost())->getPostItemsByLatLong($inputs);
           // dd($siteData);
            $siteList = [];
            $images = [];

            if(!empty($siteData)) {
                foreach($siteData as $key  => $site) {

                    $status = '0';

                    if(!empty($inputs['user_id']) && $inputs['user_id'] > 0) {
                        $favoritePosts = Favorite::where('user_id',$inputs['user_id'])->where('site_post_id',$site->id)->first();
                        $status = !empty($favoritePosts) ? $favoritePosts->status  : '0';
                    }

                    $imgArr = SitePostImage::where('site_post_id', $site->id)->pluck('image');
                    
                    $speed = 60; //km per hour
                    $distance = $site->distance;

                    $minutes = ($distance % $speed) / $speed * 60; //time = distance/speed

                    $time = convertMintsToHoursMins($minutes, $format = '%02d:%02d');

                    $siteList[$key] = [
                            'id' => $site->id,
                            'item_type' => $site->item_type,
                            'project_name' => $site->project_name,
                            'cuyds_of_dirt_min' => $site->cuyds_of_dirt_min,
                            'cuyds_of_dirt_max' => $site->cuyds_of_dirt_max,
                            'address' => $site->address,
                            'description' => $site->description,
                            'latitude' => $site->latitude,
                            'longitude' => $site->longitude,
                            'distance' => $site->distance,
                            'is_fav' => $status,
                            'estimate_time' => $time,
                            'images' => $imgArr // or by using in side another for eachloop  $images,
                        ];
                } 
                if (!empty($siteList)) {
                    return jsonResponse(true, 200, "List of site post", [], $siteList);
                } else {
                    return jsonResponse(false, 200, "No data found",[],[]);
                }
            }
            else {
                return jsonResponse(false, 200, "No data found",[],[]);

            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }



    public function destroyPostSite(Request $request, $id = null) {
        try {
            if ($request->isMethod('delete')) {
                $deleteUser = (new SitePost())->find($id);
                if ($deleteUser) {
                    $deleteUser->delete();
                    SitePostImage::where('site_post_id', $id)->delete();
                    return jsonResponse(true, 200, "Post Site Data Deleted Sucessfully ",[],[]);
                } else {
                    return jsonResponse(false, 404, "No Post Site Id Found?",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }



        public function updateStatus(Request $request, $id = null, $statusVal = null) {
            try {
                $status = $statusVal == '1' ? '0' : '1';
                if ($request->isMethod('post')) {
                    $statusUser = (new SitePost())->where('id', $id)->update(['status' => $status]);
                    if ($statusUser) {
                        return jsonResponse(true, 200, "Status Updated Sucessfully ",[],[]);
                    } else {
                        return jsonResponse(false, 404, "No Post Site Id Found?",[],[]);
                    }
                }
                return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
            } catch (\Exception $e) {
                echo $e->getMessage();
                die;
                // something went wrong
            }

        }





}
