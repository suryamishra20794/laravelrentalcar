<?php

namespace App\Http\Controllers\Api;

use App\Models\CarInformation;
use Illuminate\Http\Request;
use Hash;
use DB;
use League\Flysystem\Exception;
use Validator;


class CarInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if ($request->isMethod('get')) {
                $inputs = $request->all();
                $getCarInfoList = (new CarInformation())->orderBy('id', 'DESC')->get();
                $getCarInfoListArr = isset($getCarInfoList) && $getCarInfoList != '' ? $getCarInfoList->toArray() : [];
                if ($getCarInfoList) {
                    return jsonResponse(true, 200, "Car Info  List", [], $getCarInfoListArr);
                } else {
                    return jsonResponse(false, 200, "No data found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  /**
     * @Create Site post method
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $isFavData = [];
                $validator = Validator::make($inputs, [
                    'user_id' => 'required',
                    'car_type' => 'required',
                    'car_name' => 'required',
                    'car_number' => 'required|string',
                    'car_image' => 'required|mimes:jpg,jpeg,png,bmp|max:20000',
                    'car_model_number' => 'required',
                    'seats' => 'required',
                    'fuel_type' => 'required',
                    'location' => 'required',
                    'mileage' => 'required',
                    'satellite_navigation' => 'required',
                    'air_conditioning' => 'required',
                    'automatic_transmission' => 'required',
                    'how_many_year_old' => 'required',
                    'additional_information' => 'required'

                ],[
                    'image.mimes' => 'Only jpeg,png and bmp images are allowed',
                    'image.max' => 'Sorry! Maximum allowed size for an image is 20MB',
                ]);

                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $image = $request->file('car_image');
                if (!empty($image)){
                    $imagePath = \Config::get('constants.CAR_INFO.IMAGE');
                    $imageUrl = imageBashUrl($imagePath);
                    $input['image_name'] = time(). uniqid() . '.' . $image->getClientOriginalExtension();
                    $imagePath = public_path(\Config::get('constants.CAR_INFO.IMAGE'));
                    $image->move($imagePath, $input['image_name']);
                }
                $arrCarInfo = array(
                    'user_id' => isset($inputs['user_id']) && $inputs['user_id'] != '' ? $inputs['user_id'] : '',
                    'car_type' => isset($inputs['car_type']) && $inputs['car_type'] != '' ? $inputs['car_type'] : '',
                    'status' => isset($inputs['status']) && $inputs['status'] != '' ? $inputs['status'] : '',
                    'car_name' => isset($inputs['car_name']) && $inputs['car_name'] != '' ? $inputs['car_name'] : '',
                    'car_number' => isset($inputs['car_number']) && $inputs['car_number'] != '' ? $inputs['car_number'] : '',
                    'car_model_number' => isset($inputs['car_model_number']) && $inputs['car_model_number'] != '' ? $inputs['car_model_number'] : '',
                    'seats' => isset($inputs['seats']) && $inputs['seats'] != '' ? $inputs['seats'] : '',
                    'fuel_type' => isset($inputs['fuel_type']) && $inputs['fuel_type'] != '' ? $inputs['fuel_type'] : '',
                    'location' => isset($inputs['location']) && $inputs['location'] != '' ? $inputs['location'] : '',
                    'mileage' => isset($inputs['mileage']) && $inputs['mileage'] != '' ? $inputs['mileage'] : '',
                    'longitude' => isset($inputs['longitude']) && $inputs['longitude'] != '' ? $inputs['longitude'] : '',
                    'latitude' => isset($inputs['latitude']) && $inputs['latitude'] != '' ? $inputs['latitude'] : '',
                    'satellite_navigation' => isset($inputs['satellite_navigation']) && $inputs['satellite_navigation'] != '' ? $inputs['satellite_navigation'] : '',
                    'air_conditioning' => isset($inputs['air_conditioning']) && $inputs['air_conditioning'] != '' ? $inputs['air_conditioning'] : '',
                    'how_many_year_old' => isset($inputs['how_many_year_old']) && $inputs['how_many_year_old'] != '' ? $inputs['how_many_year_old'] : '',
                    'automatic_transmission' => isset($inputs['automatic_transmission']) && $inputs['automatic_transmission'] != '' ? $inputs['automatic_transmission'] : '',
                    'additional_information' => isset($inputs['additional_information']) && $inputs['additional_information'] != '' ? $inputs['additional_information'] : '',
                    'car_image' => isset($input['image_name']) && $input['image_name'] != '' ? $imageUrl . '/' . $input['image_name'] : '',
                    'created_at' => date('y-m-d h:i:s'),
                    'updated_at' => date('y-m-d h:i:s')
                );                
                $lastInsertId = (new CarInformation())->insertGetId($arrCarInfo);
                DB::commit();
                if (!empty($lastInsertId)) {
                    return jsonResponse(true, 200, "Your car info had been added successfully", [], []);
                } else {
                    return jsonResponse(false, 200, "Your car info could not be added successfully",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CarInformation  $carInformation
     * @return \Illuminate\Http\Response
     */
 
    public function detail(Request $request, $car_info_id = null, $user_id = null){
        try {
            $inputs = $request->all();
            if(!empty($inputs['car_info_id']) && $inputs['car_info_id'] > 0){
                $inputs['car_info_id'];
            } else {
                $inputs['car_info_id'] = $car_info_id;
            }
            if(!empty($inputs['user_id']) && $inputs['user_id'] > 0){
                $inputs['user_id'];
            } else {
                $inputs['user_id'] = $user_id;
            }
            $validator = Validator::make($inputs, [
                'car_info_id' => 'required',
            ]);
            //dd($inputs);
            if($validator->fails()){
                return jsonResponse(false, 200, "",$validator->errors(),[]);
            }
            $carInfoData = CarInformation::where('id',$inputs['car_info_id'])->first();
            if($carInfoData){
                $carInfoDataList = [
                    'id' => $carInfoData->id,
                    'user_id' => $carInfoData->user_id,
                    'car_type' => $carInfoData->car_type,
                    'car_name' => $carInfoData->car_name,
                    'car_number' => $carInfoData->car_number,
                    'car_model_number' => $carInfoData->car_model_number,
                    'seats' => $carInfoData->seats,
                    'fuel_type' => $carInfoData->fuel_type,
                    'location' => $carInfoData->location,
                    'mileage' => $carInfoData->mileage,
                    'longitude'=> $carInfoData->longitude,
                    'latitude'=> $carInfoData->latitude,
                    'satellite_navigation' => $carInfoData->satellite_navigation,
                    'air_conditioning' => $carInfoData->air_conditioning,
                    'automatic_transmission' => $carInfoData->automatic_transmission,
                    'how_many_year_old' => $carInfoData->how_many_year_old,
                    'additional_information' => $carInfoData->additional_information,
                    'status' => $carInfoData->status,
                    'car_image' => $carInfoData->car_image,
                    'created_at' => $carInfoData->created_at
                ];
                if (!empty($carInfoDataList)) {
                    return jsonResponse(true, 200, "Site post details", [], $carInfoDataList);
                } else {
                    return jsonResponse(false, 200, "No Site Post Id Found",[],[]);
                }
            }
            else {
                return jsonResponse(false, 200, "No Site Post Id Found",[],[]);
            }
  
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CarInformation  $carInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(CarInformation $carInformation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CarInformation  $carInformation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $car_info_id= null, $user_id = null)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                if(!empty($inputs['car_info_id']) && $inputs['car_info_id'] > 0){
                    $inputs['car_info_id'];
                } else {
                    $inputs['car_info_id'] = $car_info_id;
                }
                if(!empty($inputs['user_id']) && $inputs['user_id'] > 0){
                    $inputs['user_id'];
                } else {
                    $inputs['user_id'] = $user_id;
                }
                $validator = Validator::make($inputs, [
                    'user_id' => 'required',
                    'car_type' => 'required',
                    'car_name' => 'required',
                    'car_number' => 'required|string',
                    'car_image' => 'required|mimes:jpg,jpeg,png,bmp|max:20000',
                    'car_model_number' => 'required',
                    'seats' => 'required',
                    'fuel_type' => 'required',
                    'location' => 'required',
                    'mileage' => 'required',
                    'satellite_navigation' => 'required',
                    'air_conditioning' => 'required',
                    'automatic_transmission' => 'required',
                    'how_many_year_old' => 'required',
                    'additional_information' => 'required'

                ],[
                    'image.mimes' => 'Only jpeg,png and bmp images are allowed',
                    'image.max' => 'Sorry! Maximum allowed size for an image is 20MB',
                ]);

                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $image = $request->file('car_image');
                if (!empty($image)){
                    $imagePath = \Config::get('constants.CAR_INFO.IMAGE');
                    $imageUrl = imageBashUrl($imagePath);
                    $input['image_name'] = time(). uniqid() . '.' . $image->getClientOriginalExtension();
                    $imagePath = public_path(\Config::get('constants.CAR_INFO.IMAGE'));
                    $image->move($imagePath, $input['image_name']);
                }
                $arrCarInfo = array(
                    'user_id' => isset($inputs['user_id']) && $inputs['user_id'] != '' ? $inputs['user_id'] : '',
                    'car_type' => isset($inputs['car_type']) && $inputs['car_type'] != '' ? $inputs['car_type'] : '',
                    'status' => isset($inputs['status']) && $inputs['status'] != '' ? $inputs['status'] : '',
                    'car_name' => isset($inputs['car_name']) && $inputs['car_name'] != '' ? $inputs['car_name'] : '',
                    'car_number' => isset($inputs['car_number']) && $inputs['car_number'] != '' ? $inputs['car_number'] : '',
                    'car_model_number' => isset($inputs['car_model_number']) && $inputs['car_model_number'] != '' ? $inputs['car_model_number'] : '',
                    'seats' => isset($inputs['seats']) && $inputs['seats'] != '' ? $inputs['seats'] : '',
                    'fuel_type' => isset($inputs['fuel_type']) && $inputs['fuel_type'] != '' ? $inputs['fuel_type'] : '',
                    'location' => isset($inputs['location']) && $inputs['location'] != '' ? $inputs['location'] : '',
                    'mileage' => isset($inputs['mileage']) && $inputs['mileage'] != '' ? $inputs['mileage'] : '',
                    'longitude' => isset($inputs['longitude']) && $inputs['longitude'] != '' ? $inputs['longitude'] : '',
                    'latitude' => isset($inputs['latitude']) && $inputs['latitude'] != '' ? $inputs['latitude'] : '',
                    'satellite_navigation' => isset($inputs['satellite_navigation']) && $inputs['satellite_navigation'] != '' ? $inputs['satellite_navigation'] : '',
                    'air_conditioning' => isset($inputs['air_conditioning']) && $inputs['air_conditioning'] != '' ? $inputs['air_conditioning'] : '',
                    'how_many_year_old' => isset($inputs['how_many_year_old']) && $inputs['how_many_year_old'] != '' ? $inputs['how_many_year_old'] : '',
                    'automatic_transmission' => isset($inputs['automatic_transmission']) && $inputs['automatic_transmission'] != '' ? $inputs['automatic_transmission'] : '',
                    'additional_information' => isset($inputs['additional_information']) && $inputs['additional_information'] != '' ? $inputs['additional_information'] : '',
                    'car_image' => isset($input['image_name']) && $input['image_name'] != '' ? $imageUrl . '/' . $input['image_name'] : '',
                    'created_at' => date('y-m-d h:i:s'),
                    'updated_at' => date('y-m-d h:i:s')
                );   
                    
                    $updatedCatData = (new CarInformation())->where('id', $inputs['car_info_id'])->update($arrCarInfo);
                    DB::commit();
                    if (!empty($updatedCatData) && !empty($updatedCatData)) {
                        return jsonResponse(true, 200, "Your Car Info updated successfully", [], []);
                    } else {
                        return jsonResponse(false, 200, "Your Car Info could not be updated successfully?",[],[]);
                    }
                } else {
                    return jsonResponse(false, 200, "No Car Info Id Found");
                }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            echo $e->getLine();
            die;
            // something went wrong
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CarInformation  $carInformation
     * @return \Illuminate\Http\Response
     */
    public function destroyCarInfo(Request $request, $id = null) {
        try {
            if ($request->isMethod('delete')) {
                $carInfoDelete = (new CarInformation())->find($id);
                if ($carInfoDelete) {
                    $carInfoDelete->delete();
                    return jsonResponse(true, 200, "Car Info Data Deleted Sucessfully ",[],[]);
                } else {
                    return jsonResponse(false, 404, "No Car Info Id Found?",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }



        public function updateStatus(Request $request, $id = null, $statusVal = null) {
            try {
                $status = $statusVal == '1' ? '0' : '1';
                if ($request->isMethod('post')) {
                    $statusUser = (new CarInformation())->where('id', $id)->update(['status' => $status]);
                    if ($statusUser) {
                        return jsonResponse(true, 200, "Status Updated Sucessfully ",[],[]);
                    } else {
                        return jsonResponse(false, 404, "No Car Info Id Found?",[],[]);
                    }
                }
                return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
            } catch (\Exception $e) {
                echo $e->getMessage();
                die;
                // something went wrong
            }

        }
}
