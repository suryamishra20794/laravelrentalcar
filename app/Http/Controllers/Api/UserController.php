<?php

namespace App\Http\Controllers\Api;

use App\Models\Driver;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\DriverDocument;

use App\Models\Contact;
use Hash;
use DB;
use League\Flysystem\Exception;
use Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth; 

class UserController extends Controller
{
    /**
     * @Get all user function
     * @param Request $request
     *
     */

    public function index(Request $request)
    {
        try {
            if ($request->isMethod('get')) {
                $inputs = $request->all();
                $getUserList = (new User())->where('user_role', '3')->orderBy('id', 'DESC')->get();
                $getUserListArr = isset($getUserList) && $getUserList != '' ? $getUserList->toArray() : [];
                if ($getUserList) {
                    return jsonResponse(true, 200, "User List", [], $getUserListArr);
                } else {
                    return jsonResponse(false, 200, "No user data found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }



      /**
     * @Get all user function
     * @param Request $request
     *
     */

    public function subAdminList(Request $request)
    {
        try {
            if ($request->isMethod('get')) {
                $inputs = $request->all();
                $getUserList = (new User())->where('user_role', '2')->orderBy('id', 'DESC')->get();
                $getUserListArr = isset($getUserList) && $getUserList != '' ? $getUserList->toArray() : [];
                if ($getUserList) {
                    return jsonResponse(true, 200, "User List", [], $getUserListArr);
                } else {
                    return jsonResponse(false, 200, "No user data found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }

    
    /**
     * @User Register post function
     * @param Request $request
     *
     */

    public function userRegister(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
              
                $validator = Validator::make($inputs, [
                    'email' => 'required|email|unique:users',
                    'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                    'password_confirmation' => 'required|min:6',                    'dob' => 'required:string',
                    'first_name' => 'required|string',
                    'last_name' => 'required|string',
                    'street_address' => 'required|string',
                   // 'street_address_2' => 'required|string',
                    'country' => 'required|string',
                    'city' => 'required|string',
                    'state' => 'required|string',
                    'zipcode' => 'required|string',
                    'user_type' => 'required|string',
                    'phone_code' => 'required',
                    'phone_number' => 'required|unique:users',
                    'zipcode' => 'required',
                    //'alert_message_type' => 'required',
                    'image' => 'image|max:5000',
                ]);
                
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }

                $userEmailExists = (new User())->getExistUserEmail($inputs);
                $userMobileNumberExists = (new User())->getExistUserMobileNumber($inputs);
                if (!empty($userMobileNumberExists->phone_number)) {
                        return jsonResponse(false, 200, "This phone number already taken!", [], []);
                    
                } else if (!empty($userEmailExists->email)) {
                        return jsonResponse(false, 200, "This email already taken!", [], []);
                    
                } else {
                    $image = $request->file('image');
                    if (!empty($image)) {
                        $input['file_name'] = time() . '.' . $image->getClientOriginalExtension();
                        $destinationPath = public_path(\Config::get('constants.USER.PROFILE_PIC'));
                        $image->move($destinationPath, $input['file_name']);
                    }
                    $userImagePath = \Config::get('constants.USER.PROFILE_PIC');
                    $imageUrl = imageBashUrl($userImagePath);
                    $inputs['image'] = isset($input['file_name']) && $input['file_name'] != '' ? $imageUrl . '/' . $input['file_name'] : '';
                    $inputs['phone_verified'] = "0";
                    $inputs['user_role'] = !empty($inputs['user_type']) && $inputs['user_type'] == 'sub_admin' ? '2' : '3';
                    $password = trim($inputs['password']);
                    unset($inputs['password_confirmation']);
                    unset($inputs['error']);
                    $inputs['password'] = Hash::make($password);
                    $inputs['created_at'] = date("Y-m-d H:i:s");
                    $inputs['updated_at'] = date("Y-m-d H:i:s");
                    $lastInsertId = (new User())->insertGetId($inputs);
                    $registerUser = (new User())->getRegisterUserInfo($lastInsertId);
                    $token =  $registerUser->createToken($inputs['email'].'_Token')->plainTextToken;
                    $getUserInfoArr = isset($registerUser) && $registerUser != '' ? $registerUser->toArray() : [];
                    $url = url('api/match/email/verification?').\Illuminate\Support\Arr::query(['user_id' => $lastInsertId]);
                    $userName = $inputs['first_name'] .' '. $inputs['last_name'];
                    $subject = !empty($inputs['subject']) ? $inputs['subject'] : 'User Verification';
                    $content = !empty($inputs['content']) ? $inputs['content'] : 'User registation is successfully done, Please verify email';
                    // \Mail::send('email_design.email_verification', compact('content','subject', 'userName','url' ), function($email) use ($inputs,$subject,$userName,$url)
                    // {
                    //     $email->from($inputs['email'], $userName);
                    //     $email->to($inputs['email'])->subject($subject);
                    // });
                    DB::commit();
                    return jsonResponseTokenWithData(true, 200, "You are successfully registered, Please check your email and and click link to verify it", [], $token, $userName);
                    
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
           // $e->getLine();
            DB::rollback();
            echo 'Message: ' . $e->getMessage() .'line' . $e->getLine();
            die;
        }
    }

    /**
     * @User Login post function
     * @param Request $request
     */

    public function userLogin(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'email' => 'required',
                    'password' => 'required',
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $getUserInfo = (new User())->getLogin($inputs);
             
                
                $getUserInfoArr = isset($getUserInfo) && $getUserInfo != '' ? $getUserInfo->toArray() : [];
                
                if(!empty($getUserInfoArr)) {

                    $activeUserInfo = DB::table('personal_access_tokens')->where("tokenable_id",$getUserInfoArr['id'])->first();
                    $password = trim($inputs['password']);
                    $userName = $getUserInfoArr['first_name'] .' '. $getUserInfoArr['last_name'];
                    if (Hash::check($password, $getUserInfo['password']) && !empty($getUserInfoArr)) {

                        // if(!empty($activeUserInfo)) {

                        //     return response()->json([
                        //         'statue' => false,
                        //         'status_code' => 200, 
                        //         'message' => "This user already logged in somewhere else, please try after some time.",
                        //         'error' =>  [],
                        //         'result' => '', 
                        //         'user_name' => '', 
                        //         'user_id' => '',
                        //         'user_role' => ''
                        //     ]);

                        //     return jsonResponse(false, 200, "This user already logged in somewhere else, please try after some time.",[],[]);
                        // } 
                        //else {
                            $registerInfo = array(
                                'device_token' => isset($inputs['device_token']) && $inputs['device_token'] != '' ? $inputs['device_token'] : $getUserInfo['device_token'],
                                'device_type' => isset($inputs['device_type']) && $inputs['device_type'] != '' ? $inputs['device_type'] : $getUserInfo['device_type'],
                                'latitude' => isset($inputs['latitude']) && $inputs['latitude'] != '' ? $inputs['latitude'] : $getUserInfo['latitude'],
                                'longitude' => isset($inputs['longitude']) && $inputs['longitude'] != '' ? $inputs['longitude'] : $getUserInfo['longitude'],
                                'created_at' => date('y-m-d h:i:s'),
                                'updated_at' => date('y-m-d h:i:s')
                            );
                            if (isset($getUserInfo['phone_verified']) && $getUserInfo['phone_verified'] == '0') {
                                (new User())->where('id', $getUserInfo['id'])->update($registerInfo);
                            } else {
                                (new User())->where('id', $getUserInfo['id'])->update($registerInfo);
                            }
                            // 1 is admin 
                            if ($getUserInfo->user_role == '1' || $getUserInfo->user_role == '2')  {
                                $role = 'admin';
                                $token =  $getUserInfo->createToken($inputs['email'].'_AdminToken', ['server:admin'])->plainTextToken;
    
                            } else {
                                $role = '';
                                $token =  $getUserInfo->createToken($inputs['email'].'_Token',[''])->plainTextToken;
                            }
    
                            DB::commit();
                           // return jsonResponseTokenWithData(true, 200, "Logged In successfully", [], $token, $userName, $getUserInfo->id, $role);
                            return response()->json([
                                'statue' => true,
                                'status_code' => 200, 
                                'message' => "Logged In successfully",
                                'error' =>  [],
                                'result' => $token, 
                                'user_name' => $userName, 
                                'user_id' => $getUserInfo->id,
                                'user_role' => $role
                            ]);
                            //return jsonResponse(true, 200, "User login successfully", [], $getUserInfoArr);
                        //}
                       
                    } else {
                        //return jsonResponse(false, 200, "Please enter valid email or password",[],[]);
                        return response()->json([
                            'statue' => false,
                            'status_code' => 200, 
                            'message' => "Please enter valid email or password",
                            'error' =>  [],
                            'result' => '', 
                            'user_name' => '', 
                            'user_id' => '',
                            'user_role' => ''
                        ]);
                    }
                } else {
                   // return jsonResponse(false, 200, "Please enter valid email or password",[],[]);

                    return response()->json([
                        'statue' => false,
                        'status_code' => 200, 
                        'message' => "Please enter valid email or password",
                        'error' =>  [],
                        'result' => '', 
                        'user_name' => '', 
                        'user_id' => '',
                        'user_role' => ''
                    ]);
                }
                    
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }


    /**
     * @User Login post function
     * @param Request $request
     */

    public function userLoginOld(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'email' => 'required',
                    'password' => 'required',
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $getUserInfo = (new User())->getLogin($inputs);
             
                
                $getUserInfoArr = isset($getUserInfo) && $getUserInfo != '' ? $getUserInfo->toArray() : [];
                
                if(!empty($getUserInfoArr)) {

                    $activeUserInfo = DB::table('personal_access_tokens')->where("tokenable_id",$getUserInfoArr['id'])->first();
                    $password = trim($inputs['password']);
                    $userName = $getUserInfoArr['first_name'] .' '. $getUserInfoArr['last_name'];
                    if (Hash::check($password, $getUserInfo['password']) && !empty($getUserInfoArr)) {

                        if(!empty($activeUserInfo)) {

                            return response()->json([
                                'statue' => false,
                                'status_code' => 200, 
                                'message' => "This user already logged in somewhere else, please try after some time.",
                                'error' =>  [],
                                'result' => '', 
                                'user_name' => '', 
                                'user_id' => '',
                                'user_role' => ''
                            ]);

                            return jsonResponse(false, 200, "This user already logged in somewhere else, please try after some time.",[],[]);
                        } 
                        else {
                            $registerInfo = array(
                                'device_token' => isset($inputs['device_token']) && $inputs['device_token'] != '' ? $inputs['device_token'] : $getUserInfo['device_token'],
                                'device_type' => isset($inputs['device_type']) && $inputs['device_type'] != '' ? $inputs['device_type'] : $getUserInfo['device_type'],
                                'latitude' => isset($inputs['latitude']) && $inputs['latitude'] != '' ? $inputs['latitude'] : $getUserInfo['latitude'],
                                'longitude' => isset($inputs['longitude']) && $inputs['longitude'] != '' ? $inputs['longitude'] : $getUserInfo['longitude'],
                                'created_at' => date('y-m-d h:i:s'),
                                'updated_at' => date('y-m-d h:i:s')
                            );
                            if (isset($getUserInfo['phone_verified']) && $getUserInfo['phone_verified'] == '0') {
                                (new User())->where('id', $getUserInfo['id'])->update($registerInfo);
                            } else {
                                (new User())->where('id', $getUserInfo['id'])->update($registerInfo);
                            }
                            // 1 is admin 
                            if ($getUserInfo->user_role == '1' || $getUserInfo->user_role == '2')  {
                                $role = 'admin';
                                $token =  $getUserInfo->createToken($inputs['email'].'_AdminToken', ['server:admin'])->plainTextToken;
    
                            } else {
                                $role = '';
                                $token =  $getUserInfo->createToken($inputs['email'].'_Token',[''])->plainTextToken;
                            }
    
                            DB::commit();
                           // return jsonResponseTokenWithData(true, 200, "Logged In successfully", [], $token, $userName, $getUserInfo->id, $role);
                            return response()->json([
                                'statue' => true,
                                'status_code' => 200, 
                                'message' => "Logged In successfully",
                                'error' =>  [],
                                'result' => $token, 
                                'user_name' => $userName, 
                                'user_id' => $getUserInfo->id,
                                'user_role' => $role
                            ]);
                            //return jsonResponse(true, 200, "User login successfully", [], $getUserInfoArr);
                        }
                       
                    } else {
                        //return jsonResponse(false, 200, "Please enter valid email or password",[],[]);
                        return response()->json([
                            'statue' => false,
                            'status_code' => 200, 
                            'message' => "Please enter valid email or password",
                            'error' =>  [],
                            'result' => '', 
                            'user_name' => '', 
                            'user_id' => '',
                            'user_role' => ''
                        ]);
                    }
                } else {
                   // return jsonResponse(false, 200, "Please enter valid email or password",[],[]);

                    return response()->json([
                        'statue' => false,
                        'status_code' => 200, 
                        'message' => "Please enter valid email or password",
                        'error' =>  [],
                        'result' => '', 
                        'user_name' => '', 
                        'user_id' => '',
                        'user_role' => ''
                    ]);
                }
                    
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }

 /**
     * @User Login post function
     * @param Request $request
     */

    public function userLogout(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                $inputs = $request->all();
                
                $tokenIds = explode("|",$inputs['auth_token']);
                $tokenId  = $tokenIds[0];
                //dd($tokenId);

                //Auth::user()->tokens()->delete();
                Auth::user()->tokens()->where('id', $tokenId[0])->delete();
                //$user->tokens()->where('id', $tokenId)->delete();
                return jsonResponse(true, 200, "Logged Out Successfully",[],[]);
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }


    /**
     * @User Change Password post function
     * @param Request $request
     * @return mixed
     */


    public function changePassword(Request $request, $userId=null)
    {

        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                if(!empty($inputs['user_id'])) {
                    $inputs['user_id'];
                } else {
                    $inputs['user_id'] = $userId;
                }
                $validator = Validator::make($inputs, [
                    'old_password' => 'required',
                    'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                    'password_confirmation' => 'required|min:6',
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $getUserInfo = (new User())->getLoginUser($inputs);
                $newPassword = Hash::make(trim($inputs['password']));
                $oldPassword = trim($inputs['old_password']);
                if(!empty($getUserInfo)){
                    if (Hash::check(trim($oldPassword), $getUserInfo['password']) && !empty($getUserInfo)) {
                        $updateUserPassword = (new User())->where('id', $getUserInfo['id'])->update(['password' => $newPassword]);
                       
                        DB::commit();
                        if ($updateUserPassword) {
                            return jsonResponse(true, 200, "You have successfully channged your password.",[],[]);
                            
                        } else {
                            return jsonResponse(false, 200, "Your password could not be changed successfully please try again?",[],[]);
                            
                        }
                    } else {
                        return jsonResponse(false, 200, "Your old password does not match!",[],[]);
                       
                    }
                }else {
                    return jsonResponse(false, 200, "Your old password does not match!",[],[]);
                   
                }
               

            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }


    /**
     * @Resend otp  post function
     * @param Request $request
     * @return mixed
     */
    public function resentEmailVerificationLink(Request $request)
    {

        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'email' => 'required|email',
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }

                $getUserInfo = (new User())->getExistUserEmail($inputs);
                if(!empty($getUserInfo)){
                    $url = url('api/match/email/verification?').\Illuminate\Support\Arr::query(['user_id' => $getUserInfo->id]);
                    $userName = $getUserInfo->first_name .' '. $getUserInfo->last_name;
                    $subject = !empty($inputs['subject']) ? $inputs['subject'] : 'Email Verification';
                    $content = !empty($inputs['content']) ? $inputs['content'] : 'Please verify email';
                    \Mail::send('email_design.email_verification', compact('content','subject', 'userName','url' ), function($email) use ($inputs,$subject,$userName,$url)
                    {
                        $email->from($inputs['email'], $userName);
                        $email->to($inputs['email'])->subject($subject);
                    });
                    $updateUserPassword = (new User())->where('id', $getUserInfo['id'])->update(['email_verified' => '1']);
                    DB::commit();
                    if ($updateUserPassword && !empty($getUserInfo)) {
                        return jsonResponse(true, 200, "Email verification link sent on your email",[],[]);
                        
                    } else {
                        return jsonResponse(false, 200, "Oops! something went wrong, please try again?",[],[]);
                        
                    }
                }else {
                    return jsonResponse(false, 200, "This email does not exist our records",[],[]);
                
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }


    /**
     * @Match otp  post method
     * @param Request $request
     * @return mixed
     */

    public function matchEmailVerification(Request $request)
    {
        try {
            if ($request->isMethod('post') || $request->isMethod('get')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'user_id' => 'required',
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $getUserInfo = (new User())->getLoginUser($inputs);
                if (!empty($getUserInfo)) {
                    (new User())->where('id', $getUserInfo['id'])->update(['email_verified' => '1']);
                    DB::commit();
                    return jsonResponse(true, 200, "Your email verification is successfully done",[],[]);
                    
                } else {
                    return jsonResponse(false, 200, "Your data does not match our records",[],[]);
                    
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            echo $e->getLine();
            die;
            // something went wrong
        }
    }

    /**
     * @Get Login user profile post method
     * @param Request $request
     * @return mixed
     */

    public function getProfile(Request $request, $userId=null)
    {
        try {
            if ($request->isMethod('get') || $request->isMethod('post')) {
                $inputs = $request->all();
                if(!empty($inputs['user_id']) && $inputs['user_id'] > 0) {
                    $getUserInfo = (new User())->getLoginUser($inputs);
                } else {
                    $getUserInfo = (new User())->getRegisterUserInfo($userId);
                }
                $getUserInfoArr = isset($getUserInfo) && $getUserInfo != '' ? $getUserInfo->toArray() : [];
                if ($getUserInfo) {
                    return jsonResponse(true, 200, "User detail found", [], $getUserInfoArr);
                } else {
                    return jsonResponse(false, 200, "No User Id Found",[],[]);
                   
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }

    /**
     * @Update User Profile post method
     * @param Request $request
     * @return mixed
     */

    public function updateProfile(Request $request, $id = null)
    {
        
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();

                if(!empty($inputs['user_id']) && $inputs['user_id'] > 0){
                    $inputs['user_id'];
                } else {
                    $inputs['user_id'] = $id;
                }

                if(empty($inputs['image'])){
                   unset($inputs['image']);
                }

                $validator = Validator::make($inputs, [
                    'email' => 'unique:users,email,' . $inputs['user_id'],
                    'first_name' => 'required|string',
                    'last_name' => 'required|string',
                    'dob' => 'required:string',
                    'street_address' => 'required|string',
                    'phone_code' => 'required',
                    'country' => 'required|string',
                    'city' => 'required|string',
                    'state' => 'required|string',
                    'zipcode' => 'required|string',
                    'user_type' => 'required|string',
                    'phone_number' => "required|unique:users,phone_number," . $inputs['user_id'],
                    'zipcode' => 'required',
                    'image' => 'image|max:5000',


                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $getUserInfo = (new User())->getRegisterUserInfo($inputs['user_id']);
                $getUserInfoArr = isset($getUserInfo) && $getUserInfo != '' ? $getUserInfo->toArray() : [];
               
                if(!empty($getUserInfoArr)){
                    $image = $request->file('image');
                    if (!empty($image)) {
                        $input['file_name'] = time() . '.' . $image->getClientOriginalExtension();
                        $destinationPath = public_path(\Config::get('constants.USER.PROFILE_PIC'));
                        $image->move($destinationPath, $input['file_name']);
                    }
                    $userImagePath = \Config::get('constants.USER.PROFILE_PIC');
                    $imageUrl = imageBashUrl($userImagePath);
                    $arrUpdateProfile = array(
                        'email' => isset($inputs['email']) && $inputs['email'] != '' ? $inputs['email'] : $getUserInfo['email'],
                        'first_name' => isset($inputs['first_name']) && $inputs['first_name'] != '' ? $inputs['first_name'] : $getUserInfo['first_name'],
                        'last_name' => isset($inputs['last_name']) && $inputs['last_name'] != '' ? $inputs['last_name'] : $getUserInfo['last_name'],
                        'street_address' => isset($inputs['street_address']) && $inputs['street_address'] != '' ? $inputs['street_address'] : $getUserInfo['street_address'],
                        'street_address_2' => isset($inputs['street_address_2']) && $inputs['street_address_2'] != '' ? $inputs['street_address_2'] : $getUserInfo['street_address_2'],
                        'city' => isset($inputs['city']) && $inputs['city'] != '' ? $inputs['city'] : $getUserInfo['city'],
                        'state' => isset($inputs['state']) && $inputs['state'] != '' ? $inputs['state'] : $getUserInfo['state'],
                        'user_type' => isset($inputs['user_type']) && $inputs['user_type'] != '' ? $inputs['user_type'] : $getUserInfo['user_type'],
                        'phone_number' => isset($inputs['phone_number']) && $inputs['phone_number'] != '' ? $inputs['phone_number'] : $getUserInfo['phone_number'],
                        //'company_name' => isset($inputs['company_name']) && $inputs['company_name'] != '' ? $inputs['company_name'] : $getUserInfo['company_name'],
                        'alert_message_type' => isset($inputs['alert_message_type']) && $inputs['alert_message_type'] != '' ? $inputs['alert_message_type'] : $getUserInfo['alert_message_type'],
                        'latitude' => isset($inputs['latitude']) && $inputs['latitude'] != '' ? $inputs['latitude'] : $getUserInfo['latitude'],
                        'longitude' => isset($inputs['longitude']) && $inputs['longitude'] != '' ? $inputs['longitude'] : $getUserInfo['longitude'],
                        'image' => isset($input['file_name']) && $input['file_name'] != '' ? $imageUrl . '/' . $input['file_name'] : $getUserInfo['image'],
                        'created_at' => date('y-m-d h:i:s'),
                        'updated_at' => date('y-m-d h:i:s')
                    );
                    $updateProfile = (new User())->where('id', $inputs['user_id'])->update($arrUpdateProfile);
                    $getNewUserInfo = (new User())->getRegisterUserInfo($inputs['user_id']);
                    $getLastUserInfoArr = isset($getNewUserInfo) && $getNewUserInfo != '' ? $getNewUserInfo->toArray() : [];
                    DB::commit();
                } else {
                    return jsonResponse(false, 200, "No User Id Found",[],[]);

                }
                
                if ($updateProfile && !empty($getUserInfo)) {
                    return jsonResponse(true, 200, "Your profile updated successfully", [], $getLastUserInfoArr);
                } else {
                    return jsonResponse(false, 200, "Your Profile could not be updated successfully?",[],[]);
                    
                }

            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }

    /**
     * @Update User LatLong post method
     * @param Request $request
     * @return mixed
     */

    public function updateLatLong(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'user_id' => 'required',
                    'latitude' => 'required',
                    'longitude' => 'required|string',
                    'device_token' => 'required|string'
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $getUserInfo = (new User())->getLoginUser($inputs);
                $updateLatlong = array(
                    'latitude' => isset($inputs['latitude']) && $inputs['latitude'] != '' ? $inputs['latitude'] : $getUserInfo['latitude'],
                    'longitude' => isset($inputs['longitude']) && $inputs['longitude'] != '' ? $inputs['longitude'] : $getUserInfo['longitude'],
                    'device_token' => isset($inputs['device_token']) && $inputs['device_token'] != '' ? $inputs['device_token'] : $getUserInfo['device_token'],
                    'created_at' => date('y-m-d h:i:s'),
                    'updated_at' => date('y-m-d h:i:s')
                );
                if ($getUserInfo) {
                    $getUserInfoData = (new User())->matchDiviceToken($inputs);
                    if ($getUserInfoData) {
                        (new User())->where('id', $getUserInfo['id'])->update($updateLatlong);
                        DB::commit();
                        $userStatus = (new User())->getLoginDriverIdLatLong($inputs);
                        $userStatusArr = isset($userStatus) && $userStatus != '' ? $userStatus->toArray() : [];
                        return jsonResponse(true, 200, "latitude longitude update successfully.", [], $userStatusArr);
                    } else {
                        (new User())->where('id', $getUserInfo['id'])->update($updateLatlong);
                        DB::commit();
                        $userStatus = (new User())->getLoginDriverIdLatLong($inputs);
                        $userStatusArr = isset($userStatus) && $userStatus != '' ? $userStatus->toArray() : [];
                        return jsonResponse(true, 200, "Latitude longitude updated successfully.", [], $userStatusArr);
                    }
                } else {
                    return jsonResponse(false, 200, "Latitude longitude could not be updated successfully?",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            DB::rollback();
            //echo $e->getMessage();
            // something went wrong
        }
    }

    /**
     * @Update User Notification status post method
     * @param Request $request
     * @return mixed
     */

    public function updateNotificationStatus(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $getUserInfo = (new User())->getLoginUser($inputs);
                $updateNotificationStatus = array(
                    'notification_status' => isset($inputs['notification_status']) && $inputs['notification_status'] != '' ? $inputs['notification_status'] : $getUserInfo['notification_status'],
                    'created_at' => date('y-m-d h:i:s'),
                    'updated_at' => date('y-m-d h:i:s')
                );

                if ($getUserInfo) {
                    (new User())->where('id', $getUserInfo['id'])->update($updateNotificationStatus);
                    DB::commit();
                    return jsonResponse(true, 200, "Notification status updated successfully.");
                } else {
                    return jsonResponse(false, 200, "Notification status could not be updated successfully?");
                    
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            DB::rollback();
            //echo $e->getMessage();
            // something went wrong
        }
    }


    /**
     * @Contact us post method
     * @param Request $request
     * @return mixed
     */


    public function contactUs(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'user_id' => 'required',
                    'name' => 'required|string',
                    'email' => 'required|email',
                    'phone' => 'required',
                    'message' => 'required'
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $insertContact = (new Contact())->create($inputs);
                if ($insertContact) {
                    DB::commit();
                        return jsonResponse(true, 200, "Contact detail sent successfully",[],[]);
                } else {
                    return jsonResponse(false, 200, "Contact detail could not be sent successfully?",[],[]);
                    
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactList(Request $request)
    {

        try {
            if ($request->isMethod('get')) {
                $getContact = (new Contact())->orderBy('id', 'DESC')->get();
                $getContactListArr = isset($getContact) && $getContact != '' ? $getContact->toArray() : [];
                if ($getContactListArr) {
                    return jsonResponse(true, 200, "Contact List", [], $getContactListArr);
                } else {
                    return jsonResponse(false, 200, "No Contact data found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }


    

     /**
     * Display the specified resource.
     *
     * @param  \App\page  $specialist
     * @return \Illuminate\Http\Response
     */
    public function contactDetail(Request $request, $id)
    {
        try {
            if ($request->isMethod('get')) {
                $contact = (new Contact())->where('id',$id)->first()->toArray();
                if ($contact) {
                    return jsonResponse(true, 200, "Contact details founds", [], $contact);
                } else {
                    return jsonResponse(false, 200, "No contact id found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            //DB::rollback();
            //echo $e->getMessage();
        }
    }


     /**
     * @forget Password us post method
     * @param Request $request
     * @return mixed
     */


    public function forgetPassword(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'email' => 'required|email'
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $getUserInfo = (new User())->getLogin($inputs);
                $password = randomPassword();
                $newPassword = Hash::make(trim($password));
                $subject =  'Your Password';
                $content = 'Your password is successfully updated, Please check your email';
                if(!empty($getUserInfo)) {
                    $userName = $getUserInfo->first_name .' '. $getUserInfo->last_name;
                    $updatePassword = (new User())->where('id',$getUserInfo->id)->update(['password' => $newPassword]);
                    if ($updatePassword) {
                    \Mail::send('email_design.forget_password', compact('content','subject', 'userName','password' ), function($email) use ($inputs,$subject,$userName,$password)
                    {
                        $email->from($inputs['email'], $userName);
                        $email->to($inputs['email'])->subject($subject);
                    });
                    DB::commit();
                    return jsonResponse(true, 200, "You have successfully reset your password.Please check your email",[],[]);
                    } else {
                        return jsonResponse(false, 200, "Your password is could not reset succesfully",[],[]);
                        
                    }
                } else {
                    return jsonResponse(false, 200, "This email is not exits in our database records, Please check and and retry?",[],[]);
                }
                
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }



    public function destroyUser(Request $request, $id = null) {
        try {
            if ($request->isMethod('delete')) {
                $deleteUser = (new User())->find($id);
                if ($deleteUser) {
                    $deleteUser->delete();
                    return jsonResponse(true, 200, "User Deleted Sucessfully ",[],[]);
                } else {
                    return jsonResponse(false, 404, "No User Id Found?",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }




    public function deleteContact(Request $request, $id = null) {
        try {
            if ($request->isMethod('delete')) {
                $deleteContact = (new Contact())->find($id);
                if ($deleteContact) {
                    $deleteContact->delete();
                    return jsonResponse(true, 200, "Contact Deleted Sucessfully ",[],[]);
                } else {
                    return jsonResponse(false, 404, "No Contact Id Found?",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }



    public function updateStatus(Request $request, $id = null, $statusVal = null) {
        try {
            $status = $statusVal == '1' ? '0' : '1';
            if ($request->isMethod('post')) {
                $statusUser = (new User())->where('id', $id)->update(['status' => $status]);
                if ($statusUser) {
                    return jsonResponse(true, 200, "Status Updated Sucessfully ",[],[]);
                } else {
                    return jsonResponse(false, 404, "No User Id Found?",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }

    /**
     * @User Register post function
     * @param Request $request
     *
     */

    public function saveDocument(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
              
                $validator = Validator::make($inputs, [
                    'user_id' => 'required',
                    'drivery_lincence' => 'required|image|max:5000',
                    'experince_letter' => 'required|image|max:5000',
                    'registration_certificate' => 'required|image|max:5000'
                ]);
                
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                else {
                    $userInfo = (new User())->find($inputs['user_id']);
                    if($userInfo) {
                        DriverDocument::where('user_id', $inputs['user_id'])->delete();
                        $drivery_lincence = $request->file('drivery_lincence');
                        $experince_letter = $request->file('experince_letter');
                        $registration_certificate = $request->file('registration_certificate');
    
                        if (!empty($drivery_lincence)) {
                            $input['drivery_lincence_new'] = time() . '.' . $drivery_lincence->getClientOriginalExtension();
                            $destinationPathDriveryLincence = public_path(\Config::get('constants.DOCUMENT.DRIVERY_LINCENCE'));
                            $drivery_lincence->move($destinationPathDriveryLincence, $input['drivery_lincence_new']);
                        }
                        if (!empty($experince_letter)) {
                            $input['experince_letter_new'] = time() . '.' . $experince_letter->getClientOriginalExtension();
                            $destinationPathExperianceLetter = public_path(\Config::get('constants.DOCUMENT.EXPERINCE_LETTER'));
                            $experince_letter->move($destinationPathExperianceLetter, $input['experince_letter_new']);
                        }
                        if (!empty($registration_certificate)) {
                            $input['registration_certificate_new'] = time() . '.' . $registration_certificate->getClientOriginalExtension();
                            $destinationPathRegistrationCertificate = public_path(\Config::get('constants.DOCUMENT.REGISTRATION_CERTIFICATE'));
                            $registration_certificate->move($destinationPathRegistrationCertificate, $input['registration_certificate_new']);
                        }
    
                        $destinationPathDriveryLincence = public_path(\Config::get('constants.DOCUMENT.DRIVERY_LINCENCE'));
                        $driveryLincenceUrl = imageBashUrl($destinationPathDriveryLincence);
    
                        $destinationPathExperianceLetter = public_path(\Config::get('constants.DOCUMENT.EXPERINCE_LETTER'));
                        $experianceLetterUrl = imageBashUrl($destinationPathExperianceLetter);
    
                        $destinationPathRegistrationCertificate = public_path(\Config::get('constants.DOCUMENT.REGISTRATION_CERTIFICATE'));
                        $registrationCertificateUrl = imageBashUrl($destinationPathRegistrationCertificate);
    
                        $inputs['drivery_lincence'] = isset($input['drivery_lincence_new']) && $input['drivery_lincence_new'] != '' ? $driveryLincenceUrl . '/' . $input['drivery_lincence_new'] : '';
                        $inputs['experince_letter'] = isset($input['experince_letter_new']) && $input['experince_letter_new'] != '' ? $experianceLetterUrl . '/' . $input['experince_letter_new'] : '';
                        $inputs['registration_certificate'] = isset($input['registration_certificate_new']) && $input['registration_certificate_new'] != '' ? $registrationCertificateUrl . '/' . $input['registration_certificate_new'] : '';
    
                        $inputs['created_at'] = date("Y-m-d H:i:s");
                        $inputs['updated_at'] = date("Y-m-d H:i:s");
                        $lastInsertId = (new DriverDocument())->insertGetId($inputs);
                     
                        DB::commit();
                        return jsonResponse(true, 200, "You have successfully save.",[],[]);

                    } 

                    
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
           // $e->getLine();
            DB::rollback();
            echo 'Message: ' . $e->getMessage() .'line' . $e->getLine();
            die;
        }
    }




     /**
     * Display the specified resource.
     *
     * @param  \App\page  $specialist
     * @return \Illuminate\Http\Response
     */
    public function documentDetail(Request $request, $id)
    {
        try {
            if ($request->isMethod('get')) {
                $driverDocument = (new DriverDocument())->where('id',$id)->first()->toArray();
                if ($driverDocument) {
                    return jsonResponse(true, 200, "Details founds", [], $driverDocument);
                } else {
                    return jsonResponse(false, 200, "No document id found",[],[]);
                }
            } 
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            //DB::rollback();
            //echo $e->getMessage();
        }
    }

}
