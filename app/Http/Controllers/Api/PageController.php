<?php

namespace App\Http\Controllers\Api;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\PageRequest;
use DB;
use League\Flysystem\Exception;
use Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
class PageController extends Controller

{

    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listPage(Request $request)
    {

        try {
            if ($request->isMethod('get')) {
                $getPages = (new Page())->orderBy('id', 'DESC')->get();
                $getPageListArr = isset($getPages) && $getPages != '' ? $getPages->toArray() : [];
                if ($getPageListArr) {
                    return jsonResponse(true, 200, "Page List", [], $getPageListArr);
                } else {
                    return jsonResponse(false, 200, "No Page data found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createPage(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'title' => 'required|string',
                    'url' => 'required',
                    'description' => 'required|string'
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $slug = Str::slug($inputs['title']. "-");
                $inputs['slug'] = $slug;
                $createPages = (new Page())->create($inputs);
                if ($createPages) {
                    DB::commit();
                    return jsonResponse(true, 200, "Page created successfully done", [], []);
                } else {
                    return jsonResponse(false, 200, "Page could not created successfully",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            // something went wrong
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\page  $specialist
     * @return \Illuminate\Http\Response
     */
    public function pageDetail(Request $request, $id)
    {
        try {
            if ($request->isMethod('get')) {
                $page = (new Page())->where('id',$id)->first()->toArray();
                if ($page) {
                    return jsonResponse(true, 200, "Page details founds", [], $page);
                } else {
                    return jsonResponse(false, 200, "No page id found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            //DB::rollback();
            //echo $e->getMessage();
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pages  $specialist
     * @return \Illuminate\Http\Response
     */
    public function updatePage(Request $request,$id)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'title' => 'required|string',
                    'url' => 'required',
                    'description' => 'required|string',
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $pages = (new Page())->where('id',$id)->first();
                $getPageInfoArr = isset($pages) && $pages != '' ? $pages->toArray() : [];
                if(!empty($getPageInfoArr)){
                    //$slug = Str::slug($inputs['title']. "-");
                    //$inputs['slug'] = $slug;
                    unset($inputs['updated_at']);
                    $inputs['created_at'] = date('y-m-d h:i:s');
                    $updatePage = (new Page())->where('id',$id)->update($inputs);
                    if ($updatePage) {
                        DB::commit();
                        return jsonResponse(true, 200, "Your page updated successfully done", [], $getPageInfoArr);
                    } else {
                        return jsonResponse(false, 200, "Your page could not be updated successfully",[],[]); 
                    }
                }
                else {
                    return jsonResponse(false, 200, "No page id founds",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            // something went wrong
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pages  $specialist
     * @return \Illuminate\Http\Response
     */
    public function destroyPage(Request $request, $id)
    {
        try {
            if ($request->isMethod('delete')) {
                DB::beginTransaction();
                $deletePage = (new Page())->find($id);
                if ($deletePage) {
                    $deletePage->delete();
                    return jsonResponse(true, 200, "Page deleted sucessfully ",[],[]);
                } else {
                    return jsonResponse(false, 404, "No page id found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }


    public function updateStatus(Request $request, $id = null, $statusVal = null) {
        try {
            $status = $statusVal == '1' ? '0' : '1';
            if ($request->isMethod('post')) {
                $statusUser = (new Page())->where('id', $id)->update(['status' => $status]);
                if ($statusUser) {
                    return jsonResponse(true, 200, "Status Updated Sucessfully ",[],[]);
                } else {
                    return jsonResponse(false, 404, "No page id found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }

    }

    public function cmsPage(Request $request, $slug = null){
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $pageDetails = (new Page())->where('slug', $slug)->first();
                $pageDetailsArr = !empty($pageDetails) ?  $pageDetails->toArray() : [];
                //dd($pageDetailsArr);
                if (!empty($pageDetailsArr)) {
                    return jsonResponse(true, 200, "Page details found sucessfully ",[],$pageDetailsArr);
                } else {
                    return jsonResponse(false, 404, "No page slug found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.",[],[]);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
            // something went wrong
        }
    }
}
