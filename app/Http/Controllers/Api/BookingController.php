<?php

namespace App\Http\Controllers\Api;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\PageRequest;
use DB;
use League\Flysystem\Exception;
use Validator;
use App\Models\Booking;
use App\Models\Fares;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
class BookingController extends Controller

{

/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveBooking(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                $lastBookingId = (new Booking())->orderBy('id', 'DESC')->first();
                DB::beginTransaction();
                $inputs = $request->all();
                $validator = Validator::make($inputs, [
                    'user_id' => 'required',
                    'car_info_id' => 'required',
                    'booking_days' => 'required'
                ]);
                if($validator->fails()){
                    return jsonResponse(false, 200, "",$validator->errors(),[]);
                }
                $booking = (new Booking())->create($inputs);
                if ($booking) {
                    DB::commit();
                    return jsonResponse(true, 200, "Booking has been done successfully", [], []);
                } else {
                    return jsonResponse(false, 200, "Booking could not created successfully",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            // something went wrong
        }
    }


/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveFare(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                DB::beginTransaction();
                $inputs = $request->all();
                $faresData = (new Fares())->orderBy('id', 'DESC')->first();
                $faresDataArr = $faresData ? $faresData->toArray() : [];
               
                $bookingUpdate = '';
                $bookingSave = '';
                if($faresDataArr) {
                  $base_price = $inputs['base_price'] ? $inputs['base_price'] : $faresDataArr['base_price'];
                  $rate_per_km = $inputs['rate_per_km'] ? $inputs['rate_per_km'] : $faresDataArr['rate_per_km'];    
                  $bookingUpdate = (new Fares())->where('id', $faresData['id'])->update(['base_price' => $base_price,'rate_per_km' => $rate_per_km]);
                }
                else {
                    $bookingSave = (new Fares())->create($inputs);
                }
                if ($bookingSave || $bookingUpdate) {
                    DB::commit();
                    return jsonResponse(true, 200, "Fare has been updated successfully", [], []);
                } else {
                    return jsonResponse(false, 200, "Fare could not updated successfully",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            DB::rollback();
            echo $e->getMessage();
            // something went wrong
        }
    }


       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function booking(Request $request)
    {

        try {
            if ($request->isMethod('get')) {
                $getBooking = (new Booking())->orderBy('id', 'DESC')->get();
                $getBookingListArr = isset($getBooking) && $getBooking != '' ? $getBooking->toArray() : [];
                if ($getBookingListArr) {
                    return jsonResponse(true, 200, "Booking List", [], $getBookingListArr);
                } else {
                    return jsonResponse(false, 200, "No Booking data found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }


        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bookingUser(Request $request)
    {

        try {
            if ($request->isMethod('post')) {
                $inputs = $request->all();
                $getBooking = (new Booking())->where('user_id',$inputs['user_id'])->orderBy('id', 'DESC')->get();
                $getBookingListArr = isset($getBooking) && $getBooking != '' ? $getBooking->toArray() : [];
                if ($getBookingListArr) {
                    return jsonResponse(true, 200, "Booking List", [], $getBookingListArr);
                } else {
                    return jsonResponse(false, 200, "No Booking data found",[],[]);
                }
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }


      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fare(Request $request)
    {

        try {
            if ($request->isMethod('get')) {
                $getFare = (new Fares())->orderBy('id', 'DESC')->get();
                $getFareArr = isset($getFare) && $getFare != '' ? $getFare->toArray() : [];
                if ($getFareArr) {
                    return jsonResponse(true, 200, "Fare data", [], $getFareArr);
                } else {
                    return jsonResponse(false, 200, "No fare data found",[],[]);
                } 
            }
            return jsonResponse(false, 500, "Oops! something went wrong, server error.");
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }




}