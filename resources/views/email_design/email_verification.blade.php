
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rental Car</title>
<style type="text/css">
a {
  color: #fff;
  text-decoration: none;
}
a:hover {
  color: #656565;
}
.email_footer a {
  color: #f8261f;
}
.email_footer a:hover {
  color: #d6a540;
}
</style>
</head>
<body style="margin:0px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;color:#ffffff;line-height:20px;">
  <tr>
    <td>&nbsp;</td>
    <td width="590" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="30" align="right" style=" font-size:13px; color:#656565;padding:0 5px 0 0;"></td>
        </tr>
        <tr>
          <td style="border:solid 1px #d6a540; background:#d6a540; padding:4px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td bgcolor="#ffffff"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="39" style="text-align:center;padding-top:20px;padding-bottom:20px;background:#ffffff;color: #d6a540;text-transform: uppercase;font-weight: bold;font-size: 24px;">Dirt Mapper</td>
                    </tr>
                    <tr>
                      <td bgcolor="#d6a540"  style="font-size:14px; color:#ffffff; padding:12px 10px;  "><font style="font-size:17px; font-weight:500; color:#ffffff;   ">{{$subject}} </td>
                    </tr>

                    <tr>
                      <td bgcolor="#ffffff"  style="font-size:14px; color:#d6a540; padding:12px 10px;  "><font style="font-size:17px; font-weight:500; color:#ffffff;   "> <p><a href="{{ $url }}">Click here for verify email.</a></p> </td>
                    </tr>

                    <tr>
                      
                      <td valign="top" bgcolor="#ffffff" style="padding:12px;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="26" style="font-size:15px; font-weight:500; color:#2c2c2c;  ">Dear {{ $userName }}
                          
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td align="left"><table width="100%" border="0" bgcolor="#fff" cellspacing="1" cellpadding="6" style=" color:#656565;">
                                 <p>{!! $content !!}</p>
                              </table></td>
                          </tr>
                          <tr>
                            <td height="15"></td>
                          </tr>
                          <tr  style="color:#656565; font-size:13px; line-height:19px; ">
                            <td>Regards<br />
                              Support Team<br />
                              Rental Car</td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
		
       
		
      </table></td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
