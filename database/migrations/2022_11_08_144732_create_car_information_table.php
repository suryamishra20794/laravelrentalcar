<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_information', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('car_type');
            $table->string('car_name');
            $table->string('car_number');
            $table->string('car_image');
            $table->string('car_model_number');
            $table->string('seats');
            $table->string('fuel_type');
            $table->string('satellite_navigation');
            $table->string('air_conditioning');
            $table->string('automatic_transmission');
            $table->string('how_many_year_old');
            $table->longText('additional_information');
            $table->string('location')->nullable();
            $table->string('mileage')->nullable();
            $table->decimal('latitude', 8,6)->default('0');
            $table->decimal('longitude', 9,6)->default('0');
            $table->enum('status', ['0', '1'])->default('0')
            ->comment = '1=>active, 0=>inactive';

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_information');
    }
}
