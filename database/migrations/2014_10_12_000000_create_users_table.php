<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('dob')->unique()->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('phone_code')->nullable();
            $table->string('phone_number')->unique()->nullable();
            $table->string('image')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('district')->nullable();
            $table->string('local_market')->nullable();
            $table->string('city')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('street_address')->nullable();
            $table->decimal('latitude', 8,6)->default('0');
            $table->decimal('longitude', 9,6)->default('0');
            $table->string('notification_status')->nullable();
            $table->string('otp')->nullable();
            $table->string('device_type')->nullable();
            $table->string('device_token')->nullable();
            $table->string('street_address_2')->nullable();
            $table->enum('user_role', ['1','2','3'])->default('3')
            ->comment = '1=>admin, 2=>driver, 3=>user';
            $table->string('user_type')->nullable();
            $table->enum('status', ['0', '1'])->default('0')
            ->comment = '1=>active, 0=>inactive';
            $table->enum('alert_message_type', ['0', '1'])->default('0')
            ->comment = '1=>email, 0=>text';
            $table->enum('email_verified', ['0', '1'])->default('0')
            ->comment = '1=>Yes, 0=>No';
            $table->enum('phone_verified', ['0', '1'])->default('0')
            ->comment = '1=>YES, 0=>No';
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
