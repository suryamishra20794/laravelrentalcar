<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('car_info_id')->unsigned();
            //$table->timestamps('booking_date_and_time')->nullable();
            $table->string('booking_days')->nullable();
            $table->string('total_amount')->nullable();
            $table->string('token_amount')->nullable(); 
            $table->string('pending_balance')->nullable();
            $table->string('pick_up_location')->nullable();
            $table->string('drop_off_location')->nullable();
            $table->string('pick_up_date')->nullable();
            $table->string('drop_off_date')->nullable();
            $table->string('pick_up_time')->nullable();
            $table->string('drop_off_time')->nullable();
            $table->string('adults')->nullable();
            $table->string('kids')->nullable();
            $table->double('lat')->nullable();
            $table->double('long')->nullable();
            $table->string('distance')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('net_payable_amount')->nullable();
            $table->string('taxes_and_fees')->nullable();
            $table->enum('status', ['0', '1', '2'])->default('2')
            ->comment = '1=>success, 0=>failed, 2=>pending';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
